// import 'bootstrap';
// import 'bootstrap/js/dist/util';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/collapse';

const Rooftop = (function($) {
  function _dismissAlert(timeout = 5000) {
    let $alert = $('.alert-timeout');
    $alert.each(function() {
      $alert.find('.progress .progress-bar').css('width', 0);
      $alert.delay(timeout).slideUp();
    });
  }

  function _replaceBackground() {
    let $elements = $('[data-background]');
    $elements.each(function() {
      let $this = $(this);
      let url = $this.data('background');
      $this.css('background-image', 'url(' + url + ')');
    });
  }

  function _backToTop() {
    let $element = $('#to_top');
    $(window).scroll(function() {
      if ($(this).scrollTop() > 500) {
        $element.fadeIn(300);
      } else {
        $element.fadeOut(300);
      }
    });
    $element.click(function(o) {
      o.preventDefault();
      $('html, body').animate({scrollTop: 0}, 500);
    });
  }

  function _initAOS() {
    AOS.init({
      duration: 1000,
      once: true
    });
  }

  function adjustMainSlideHeight() {
    let $window = $(window);
    let $header = $('header.site-header');
    let $slide = $('.main-slide--full-height');

    let windowHeight = $window.height();
    let headerHeight = $header.height();
    let slideHeight = windowHeight - headerHeight;
    $slide.height(slideHeight);
  }

  function init() {
    _replaceBackground();
    adjustMainSlideHeight();
    _backToTop();
    _initAOS();
    _dismissAlert();
  }

  return {
    init,
    adjustMainSlideHeight
  };
})(jQuery);

$(function() {
  Rooftop.init();
});

let resizeTimer;
$(window).on('resize', () => {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(() => {
    Rooftop.adjustMainSlideHeight();
  }, 250);
});