<?php 

return [
    'title' => 'Mengapa Rooftop®',
    'benefit-1' => [
        'title' => 'INSULASI PANAS TERBAIK',
        'subtitle' => 'DAPAT MENOLAK PANAS HINGGA 71%',
        'description' => 'Atap Dingin ROOFTOP® terbuat dari bahan uPVC (Unplasticized Polyvinyl Chloride) berperforma tinggi dengan aditif anti UV dan heat protection kualitas terbaik. Material utama yang digunakan, uPVC, memiliki konduktifitas panas yang sangat rendah. Hasilnya, Atap Dingin ROOFTOP® tidak dapat menyalurkan panas semudah atap dengan bahan lain seperti logam atau asbes. Aditif anti UV dan heat protection spesial kami memastikan bahwa 71% dari sinar panas matahari akan dipantulkan; sedangkan sisa nya diserap dan diredam oleh sistem struktur dinding ganda yang dimiliki Atap Dingin ROOFTOP®.',
    ],
    'benefit-2' => [
        'title' => 'KEDAP SUARA',
        'subtitle' => 'MEREDAM SUARA HINGGA 15DB',
        'description' => 'Struktur dan rongga udara dari dinding ganda yang dimiliki Atap Dingin ROOFTOP® bisa meredam suara hingga sebesar 15db. Fitur ini sangat bermanfaat, khususnya pada saat musim hujan karena atap ROOFTOP® dapat membantu meredam suara tetesan air hujan secara signifikan.',
    ],
    'benefit-3' => [
        'title' => 'TIDAK AKAN PERNAH BERKARAT',
        'subtitle' => 'COCOK UNTUK IKLIM TROPIS',
        'description' => 'Komponen utama dari ROOFTOP®, yaitu uPVC dikenal sebagai bahan yang tidak dapat berkarat. Keunggulan ini akan sangat bermanfaat di negara dengan iklim tropis seperti Indonesia di mana tingkat kelembapan sangat tinggi. Dengan menggunakan Atap Dingin ROOFTOP®, anda tidak lagi perlu khawatir atas pengkaratan yang dapat terjadi karena atap ROOFTOP® dipastikan tidak akan berkarat meskipun di kondisi se-ekstrim apapun.',
    ],
    'benefit-4' => [
        'title' => 'KUAT & TAHAN LAMA',
        'subtitle' => 'MAMPU MENAHAN BEBAN HINGGA 540 KG',
        'description' => 'Kekuatan dan jangka hidup Atap Dingin ROOFTOP® berasal dari struktur dinding ganda dan penggunaan material uPVC serta aditif yang kualitas terbaik. Atap Dingin ROOFTOP® sangat kuat, memiliki properti kekuatan mekanikal yang baik dan dapat menahan beban hingga 540 kg per meter persegi.',
    ],
    'benefit-5' => [
        'title' => 'EFISIENSI WAKTU',
        'subtitle' => 'INSTALASI YANG CEPAT & MUDAH',
        'description' => 'Kekuatan dan kekokohan dari struktur atap ROOFTOP® dapat membantu menghemat waktu pemasangan dikarenakan aplikator bisa berdiri maupun berjalan di atap langsung saat proses instalasi; tidak lagi hanya terbatas berjalan di reng/gording saja. Selain dari itu, sistem interlock presisi yang dimiliki oleh Atap Dingin ROOFTOP® mempermudah proses penyambungan dan pemasangan atap. Berbeda dengan atap produk lain, Atap Dingin ROOFTOP® juga hanya memperlukan 4 SDS (self drilling screw) per meter persegi, sehingga akan lebih menghemat waktu dan biaya.',
    ],
    'benefit-6' => [
        'title' => 'EKONOMIS',
        'subtitle' => 'PERAWATAN YANG SANGAT MUDAH',
        'description' => 'Atap Dingin ROOFTOP® hanya memerlukan perawatan yang sangat minimal. Atap Dingin ROOFTOP® tidak akan pernah berkarat, sangat kuat, dan memiliki ketahanan terhadap bahan kimia yang baik. Maka dari itu anda tidak perlu khawatir lagi memikirkan biaya yang timbul untuk perawatan maupun perbaikan. Selain itu, garansi 15 tahun yang diberikan Atap Dingin ROOFTOP® menjamin bahwa atap ROOFTOP® adalah investasi terbaik untuk kebutuhan atap anda.',
    ],
    'benefit-7' => [
        'title' => 'DURABILITAS TINGGI',
        'subtitle' => 'TAHAN TERHADAP BAHAN KIMIA & BENTURAN',
        'description' => 'Selain tahan terhadap karat, material uPVC juga dikenal sebagai bahan yang memiliki ketahanan bagus terhadap bahan kimia. Material uPVC tahan terhadap berbagai jenis asam, basa, dan garam; hal inilah juga yang membuat material uPVC tidak dapat berkarat. Selain dari ketahanan terhadap bahan kimia, aditif impact modifier yang terdapat pada komposisi Atap Dingin ROOFTOP® memastikan bahwa Atap Dingin ROOFTOP® tidak akan mudah pecah pada saat terkena benturan.',
    ],
    'benefit-8' => [
        'title' => 'TEMBUS CAHAYA',
        'subtitle' => 'HEMAT ENERGI',
        'description' => 'Salah satu tipe dari produk kami, Atap Dingin ROOFTOP® Semi-Transparan (Semi-TR), dapat menembuskan cahaya hingga 20%. Dengan fitur ini, artinya anda dapat menghemat uang dengan memanfaatkan cahaya matahari untuk menyinari ruangan anda pada siang hari daripada membuang energi dan uang dengan menyalakan lampu.',
    ],
];