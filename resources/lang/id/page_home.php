<?php 

return [
    'what-is-rooftop' => [
        'p' => 'Atap Dingin ROOFTOP® adalah atap gelombang uPVC berdinding ganda pertama di Indonesia sejak tahun 2004. Dengan lebih dari 13 tahun pengalaman di pasar Indonesia maupun global, Atap Dingin ROOFTOP® telah terbukti sebagai merek yang paling bagus dan terpercaya untuk produk atap uPVC.
Atap Dingin ROOFTOP® diproduksi secara lokal oleh PT. Sumber Djaja Perkasa, sebuah perusahaan produsen plastik yang berbasis di Sidoarjo dan telah berdiri sejak tahun 1965.',
        'title' => 'Apa itu <strong>ROOFTOP®?</strong>',
        'button' => 'Pelajari ROOFTOP® Lebih Lanjut',
    ],
    'benefits' => [
        'title' => 'Kelebihan <strong>ROOFTOP®</strong>',
        '1' => '<strong>Kemampuan Meredam Panas</strong> Terbaik',
        '2' => 'Tidak Dapat <strong>Berkarat</strong>',
        '3' => 'Sangat <strong>Kokoh, Kuat, dan Tahan Lama</strong>',
        '4' => '<strong>Kemampuan Meredam Suara</strong> Yang Sangat Baik',
        '5' => 'Resisten <strong>Terhadap Bahan Kimia</strong>',
        '6' => 'Garansi Panjang <strong>15 Tahun</strong>',
        'learn-more' => 'Pelajari Lebih Lanjut',
    ],
    'reference' => [
        'title' => 'Referensi Proyek',
        'p' => '<p>Atap Dingin ROOFTOP® cocok untuk diaplikasikan ke semua tipe bangunan. Selain digunakan sebagai atap, ROOFTOP® juga dapat digunakan sebagai pelapis dinding / wall cladding. Aplikasi dari ROOFTOP® hanya dibatasi oleh imajinasi Anda.</p><p>Ribuan proyek telah menggunakan atap ROOFTOP®, di halaman ini Anda melihat beberapa referensi dari proyke yang telah terselesaikan sebagai gambaran dan inspirasi.</p>',
        'see-all-projects' => 'Lihat Semua Proyek',
    ],
    'more-links' => [
        'installation' => 'Petunjuk Instalasi',
        'distributors' => 'Temukan Distributor Terdekat',
        'contact' => 'Hubungi Kami',
    ],
];