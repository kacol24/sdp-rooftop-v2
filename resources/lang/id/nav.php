<?php
return [
    'product'     => 'Produk',
    'why-rooftop' => 'Mengapa Rooftop®',
    'project'     => 'Proyek',
    'pricelist'   => 'Daftar Harga & Dokumen Lain',
    'contact'     => 'Hubungi Kami',
    'our-company' => 'Perusahaan Kami',
    'blog'        => 'Berita & Acara',
    'distributor' => 'Distributor',
    'faq'         => 'FAQs',
    'calculator'  => 'Kalkulator',
];