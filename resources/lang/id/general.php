<?php 

return [
    'copyright' => 'Hak Cipta',
    'rights-reserveed' => 'Semua hak dilindungi.',
    'lang-en' => 'Bahasa Inggris',
    'lang-id' => 'Bahasa Indonesia',
    'or' => 'atau',
    'email-sent' => 'Terima kasih! E-mail anda berhasil terkirim. Silahkan tunggu balasan dari staff kami.',
];