<?php 

return [
    'home' => [
        'title' => 'Solusi Investasi Atap Anda',
    ],
    'product' => [
        'title' => 'Tentang Rooftop',
    ],
    'why-rooftop' => [
        'title' => 'Mengapa Rooftop',
        'compare' => 'Rooftop VS Atap Lain',
    ],
    'projects' => [
        'title' => 'Proyek Yang Sudah Terselesaikan',
    ],
    'pricelist' => [
        'title' => 'Daftar Harga & Dokumen Lain',
    ],
    'contact' => [
        'title' => 'Hubungi Kami',
    ],
    'our-company' => [
        'title' => 'Perusahaan Kami',
    ],
    'blog' => [
        'title' => 'Berita & Acara',
    ],
    'distributor' => [
        'title' => 'Distributor Resmi',
    ],
    'faq' => [
        'title' => 'FAQs',
    ],
    'calculator' => [
        'title' => 'Kalkulator Biaya Rooftop',
    ],
];