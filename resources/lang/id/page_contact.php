<?php 

return [
    'title' => 'Hubungi Kami',
    'factory' => 'Pabrik',
    'office-sby' => 'Kantor Surabaya',
    'office-jkt' => 'Kantor Jakarta',
    'form' => [
        'first-name' => 'Nama Depan',
        'last-name' => 'Nama Belakang',
        'phone' => 'No. Telepon',
        'company' => 'Perusahaan',
        'email' => 'Email',
        'subject' => 'Subyek/Judul',
        'message' => 'Pesan Anda',
        'agree' => 'Saya mengerti bahwa saya akan membagikan informasi ini dengan PT. Sumber Djaja Perkasa agar dapat memberikan informasi mengenai produk & harga.',
        'send' => 'Kirim',
    ],
];