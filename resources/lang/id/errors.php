<?php

return [
    '404' => [
        'title' => '404 <small>Halaman Tidak Ditemukan.</small>',
        'text'  => 'Maaf, halaman yang anda cari tidak dapat ditemukan. Tautan yang anda ikuti mungkin rusak, atau halaman mungkin telah dihapus.',
    ],
    '500' => [
        'title' => '500 <small>Telah Terjadi Kesalahan.</small>',
        'text'  => 'Whoops! Telah terjadi kesalahan saat memproses permintaan anda.',
    ],
    '503' => [
        'title' => '503 <small>Layanan Tidak Tersedia</small>',
        'text'  => 'Segera kembali! Kami sedang melaksanakan pemeliharaan website.',
    ],
];