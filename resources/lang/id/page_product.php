<?php 

return [
    'intro' => [
        'title' => 'Pionir Atap uPVC di Indonesia Sejak 2004',
        'text' => 'ROOFTOP® terbuat dari bahan plastik uPVC (unplasticized polyvinyl chloride) berperforma tinggi debgan tambahan aditif terbaik di kelasnya yang telah di uji dan di tes memenuhi standard iternasional. ROOFTOP® didesain untuk memberikan kenyamanan maksimal dan jangka hidup panjang untuk investasi atap Anda.',
    ],
    'specifications' => [
        'title' => 'Spesifikasi',
        'spec' => [
            'overall-width' => 'Lebar Total',
            'effective-width' => 'Lebar Efektif',
            'effective-width-overlap' => 'setelah overlap',
            'length' => 'Panjang',
            'material' => 'Material',
            'material-text' => '<em>High Performance uPVC</em> dengan aditif <em>UV Protection</em> khusus',
            'colors' => 'Warna',
            'colors-text' => 'Biru Muda<br>Putih<br>Warna Custom*',
            'roof-pitch' => 'Sudut Kemiringan',
            'purlin-spacing' => 'Jarang Reng/Gording',
            'types' => 'Tipe',
            'types-text' => 'Doff (Warna Solid)<br>Semi Transparan',
            'overall-thickness' => 'Ketebalan Total',
            'weight' => 'Berat',
        ],
        'minimum-order' => '<em>Minimum order quantity</em> berlaku',
        'recommended' => 'Disarankan',
    ],
    'performance' => [
        'title' => 'Performa',
        'subtitle' => 'Kuat dan Tahan Lama',
        'text' => 'Dikarenakan oleh konstruksi dinding ganda dan juga bahan baku berperforma tinggi yang digunakan, ROOFTOP® sangat kuat dan dapat menahan beban hingga 540 kg/m². Tidak seperti yang biasa di pikir oleh masyarakat, plastik yang digunakan oleh ROOFTOP® adalah plastik jenis uPVC, yaitu sebuah bahan plastik yang memiliki kemampuan tahan api secara natural. Dikarenakan plastik uPVC terbuat dari 50% chlorine (garam), ROOFTOP® tidak dapat mudah terbakar, tidak mudah menyebarkan api dan dapat padam dengan sendiri apabila sumber api telah dipadamkan.',
        'withstand' => 'Menahan Beban hingga 540 kg/m²',
        'fire' => 'Tidak Menyebarkan Api',
        'chemicals' => 'Tahan Bahan Kimia Rumah Tangga',
    ],
    'interlock' => [
        'title' => 'Sistem Interlock',
        'subtitle' => 'Sistem Interlock Anti-Bocor Inovatif',
        'text' => 'ROOFTOP® dilengkapi dengan sistem interlock terbaik untuk memastikan pemasangan presisi dan tidak meninggalkan celah di antara sambungan lembaran atap. Dikombinasikan dengan produk pengikat sekrup ROOFSEAL® yang inovatif, atap ROOFTOP® memberikan  kenyamanan dan ketenangan karena Anda tidak perlu khawatir dengan kebocoran yang akan terjadi dan tidak perlu lagi mengeluarkan uang tambahan untuk reparasi atau pun penggantian atap.',
    ],
    'title' => 'Produk',
    'types' => [
        'title' => 'Tipe',
        'subtitle' => 'Different Types for Different Needs',
        'text' => 'Atap Dingin ROOFTOP® tersedia dalam dua tipe sesuai dengan kebutuhan Anda.',
        'opaque' => [
            'title' => 'Doff (Opaque)',
            'text' => 'ROOFTOP® Doff memberikan perlindungan terbaik terhadap panas dan radiasi UV sinar matahari. ROOFTOP® Doff akan memblokir 100% semua sumber cahaya.',
        ],
        'semi-tr' => [
            'title' => 'Semi TR (Semi Transparent)',
            'text' => 'ROOFTOP® Semi-TR  memberikan fitur tembus cahaya yang dapat menembuskan kurang lebih 20% cahaya, sehingga akan membantu dalam penghematan energi pada saat siang hari, karena Anda dapat memanfaatkan sinar matahari alami.',
        ],
    ],
    'accessories' => [
        'title' => 'Aksesoris Penunjang',
        'top-ridge' => [
            'title' => 'Top Ridge',
            'text' => 'Aksesoris yang tepat untuk atap bentuk pelana, memberikan perlindungan ekstra terhadap kebocoran serta mempercantik tampilan atap Anda. Top Ridge ROOFTOP® terbuat dari bahan yang sama dengan atap nya, sehingga memiliki ketahanan dan umur panjang yang sebanding.',
        ],
        'spec' => [
            'width' => 'Lebar',
            'length' => 'Panjang',
            'color-options' => 'Pilihan Warna',
            'color' => [
                'white' => 'Putih',
                'light-blue' => 'Biru Muda',
            ],
            'effective' => 'efektif',
            'diameter' => 'Diameter',
            'top' => 'Atas',
            'bottom' => 'Bawah',
        ],
        'roofseal' => [
            'text' => 'Roofseal® merupakan produk pengerat sekrup inofatif kami yang didesain untuk menggantikan cincin karet yang ada di self drilling screw (SDS) . Roofseal® terbuat dari bahan material yang sama dengan atap ROOFTOP® sehinga dapat  can bertahan terhadap panasnya dan radiasu UV sinar matahari tanpa perlu khawatir menjadi getas ataupun retak seiring dengan berjalan nya waktu.',
        ],
        'gutter' => [
            'title' => 'Talang',
            'text' => 'Talang Air ROOFTOP® juga terbuat dari bahan uPVC yang sama dengan tambahan aditif anti UV berperforma tinggi, sehingga memastikan dapat menahan temperatur ekstrim dengan mudah. Talang air sangat berguna untuk mengalirkan air hujan dan melengkapi tampilan serta performa atap Anda.',
        ],
        'gutter-connector' => [
            'title' => 'Sambungan Talang',
            'text' => 'Sambungan Talang ROOFTOP® juga terbuat dari bahan kuat yang sama dengan talang air dan berfungsi sebagai penyambung antara talang untuk mengalirkan air hujan. Desain, konstruksi dan material yang digunakan menghasilkan sambungan talang yang anti bocor.',
        ],
        'gutter-endcap' => [
            'title' => 'Penutup Talang',
            'text' => 'Penutup Talang ROOFTOP® digunakan untuk menutup bagian ujung atau akhir dari talang. Dengan bahan material yang sama dengan talang, jangka umur yang dimiliki pun sama panjangnya dan resiko kebocoran sangat minim.',
        ],
    ],
    'genuine' => [
        'title' => 'Genuine ROOFTOP®',
        'subtitle' => 'Imitated by Many, Surpassed by None',
        'text' => 'ROOFTOP® memiliki hak paten design original dari atap gelombang uPVC berdinding ganda di Indonesia sejak tahun 2004 dan merupakan merek yang paling sering digunakan di pasar Indonesia. Dengan banyaknya merek di pasar yang menjual produk dengan desain yang mirip, pastikan bahwa atap ROOFTOP® yang Anda beli asli. Pastikan bahwa:',
        'handle-weight' => 'Cek Apakah Lembaran Atap dapat Menahan Beban Tanpa Mengalami Keretakan.',
        'precision' => 'Cek Desain dan Presisi dari Sistem <em>Interlock</em>',
        'laser-printed-logo' => 'Cek Logo dan Kode yang Telah di Print Dengan Laser Di Setiap Bagian Samping Lembaran ROOFTOP®',
    ],
    'installation' => [
        'title' => 'Prosedur Instalasi',
    ],
    'installation-1' => [
        'title' => 'Persiapkan Rangka Gording',
        'desc' => '- Pastikan gording yang digunakan lurus dan rata<br>- Tarik garis lurus perbaris sebelum pemasangan atap',
    ],
    'installation-2' => [
        'title' => 'Sudut Kemiringan',
        'desc' => '- Sudut kemiringan minimum adalah 15° (derajat)',
    ],
    'installation-3' => [
        'title' => 'Pemasangan Atap',
    ],
    'installation-4' => [
        'title' => 'Instalasi SDS (Self Drilling Screw) dan Roofseal®',
        'desc' => '- Pasang SDS sesuai dengan gambar tertera<small>(1 M² membutuhkan 4 SDS & 4 Roofseal®)</small>',
    ],
    'installation-5' => [
        'title' => 'Panjang Overlap',
        'desc' => '- Panjang overlap antara lembaran atap (bila dibutuhkan) adalah 30 cm (asumsi sudut kemiringan 15°)',
    ],
    'installation-6' => [
        'title' => 'Pemasangan Aksesoris Talang Air',
        'desc' => '- Pasang talang air dan aksesorisnya sesuai dengan gambar tertera',
    ],
    'installation-7' => [
        'title' => 'Aplikasi Atap Lengkung',
        'desc' => '- Pemasangan atap ROOFTOP® untuk aplikasi atap lengkung dapat dilakukan sesuai dengan gambar tertera',
    ],
];