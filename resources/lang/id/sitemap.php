<?php

return [
    'authorized-distributors' => 'Distributor resmi',
    'contact-us'              => 'Hubungi Kami',
    'factory'                 => 'Pabrik',
    'our-company'             => 'Perusahaan Kami',
    'product'                 => 'Produk',
    'other-links'             => 'Lainnya',
    'connect'                 => 'Koneksi',
    'rooftop-benefits'        => 'Kelebihan Rooftop',
    'rooftop-vs-competitor'   => 'Rooftop VS Kompetitor',
    'projects-portfolio'      => 'Portfolio Proyek',
    'estimate-cost'           => 'Perkirakan Biaya Rooftop',
];