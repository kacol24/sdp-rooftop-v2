<?php

return [
    'product.index'       => 'produk',
    'why-rooftop.index'   => 'mengapa-rooftop',
    'why-rooftop.compare' => 'rooftop-vs-atap-lain',
    'project.index'       => 'proyek',
    'project.show'        => 'proyek/{project}',
    'pricelist.index'     => 'daftar-harga-dan-dokumen-lain',
    'contact.index'       => 'hubungi-kami',
    'our-company.index'   => 'perusahaan-kami',
    'blog.index'          => 'berita-dan-acara',
    'distributor.index'   => 'distributor',
    'faq.index'           => 'faqs',
    'calculator.index'    => 'kalkulator-biaya-rooftop',
];