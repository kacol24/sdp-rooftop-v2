<?php 

return [
    'title' => 'Distributor Resmi',
    'table' => [
        'name' => 'Nama',
        'details' => 'Detail',
        'location' => 'Lokasi',
        'all-cities' => 'Semua Kota',
    ],
];