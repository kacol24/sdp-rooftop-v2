<?php 

return [
    'what-is-rooftop' => [
        'p' => 'Atap Dingin ROOFTOP® is the first uPVC, double wall corrugated roofing profile in Indonesia since 2004. With more than 13 years experience in the local as well as global market, Atap Dingin ROOFTOP® has been proven to be the best and most trusted brand for uPVC roof.
Atap Dingin ROOFTOP® is produced locally by PT. Sumber Djaja Perkasa, a plastic manufacturing company based in Sidoarjo that has been established since 1965.',
        'title' => 'What is <strong>ROOFTOP®?</strong>',
        'button' => 'Learn More About ROOFTOP®',
    ],
    'benefits' => [
        'title' => 'ROOFTOP® <strong>Benefits</strong>',
        '1' => 'Outstanding <strong>Heat Insulation Property</strong>',
        '2' => 'Will <strong>Never Corrode</strong>',
        '3' => 'Exceptionally <strong>Strong, Sturdy, and Durable</strong>',
        '4' => 'Excellent <strong>Noise Insulation Property</strong>',
        '5' => 'Superb <strong>Resistance Against Chemical</strong>',
        '6' => 'Long Warranty <strong>15 Years</strong>',
        'learn-more' => 'Learn More',
    ],
    'reference' => [
        'title' => 'Project Reference',
        'p' => '<p>Atap Dingin ROOFTOP® is suitable to be applied in any type of buildings. Besides being used for roofing purposes, ROOFTOP® can also be used for wall cladding. The application of ROOFTOP® is only limited by your imagination.</p><p>Thousands of projects have been completed using ROOFTOP®, here you can view some of the completed projects for your reference.</p>',
        'see-all-projects' => 'See All Projects',
    ],
    'more-links' => [
        'installation' => 'Installation',
        'distributors' => 'Find Your Nearest Distributor',
        'contact' => 'Contact Us',
    ],
];