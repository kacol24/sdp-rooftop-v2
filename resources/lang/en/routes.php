<?php

return [
    'product.index'       => 'product',
    'why-rooftop.index'   => 'why-rooftop',
    'why-rooftop.compare' => 'rooftop-vs-others',
    'project.index'       => 'projects',
    'project.show'        => 'projects/{project}',
    'pricelist.index'     => 'pricelist-and-other-documents',
    'contact.index'       => 'contact-us',
    'our-company.index'   => 'our-company',
    'blog.index'          => 'news-and-events',
    'distributor.index'   => 'distributors',
    'faq.index'           => 'faqs',
    'calculator.index'    => 'rooftop-cost-calculator',
];