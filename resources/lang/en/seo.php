<?php 

return [
    'home' => [
        'title' => 'Solution For Your Roofing Investment',
    ],
    'product' => [
        'title' => 'About Rooftop',
    ],
    'why-rooftop' => [
        'title' => 'Why Rooftop',
        'compare' => 'Rooftop VS Others',
    ],
    'projects' => [
        'title' => 'Our Finished Projects',
    ],
    'pricelist' => [
        'title' => 'Pricelists & Other Documents',
    ],
    'contact' => [
        'title' => 'Contact Us',
    ],
    'our-company' => [
        'title' => 'Our Company',
    ],
    'blog' => [
        'title' => 'News & Events',
    ],
    'distributor' => [
        'title' => 'Authorized Distributors',
    ],
    'faq' => [
        'title' => 'FAQs',
    ],
    'calculator' => [
        'title' => 'Rooftop Cost Calculator',
    ],
];