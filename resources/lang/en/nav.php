<?php
return [
    'product'     => 'Product',
    'why-rooftop' => 'Why Rooftop®',
    'project'     => 'Projects',
    'pricelist'   => 'Pricelists & Documents',
    'contact'     => 'Contact Us',
    'our-company' => 'Our Company',
    'blog'        => 'News & Events',
    'distributor' => 'Distributors',
    'faq'         => 'FAQs',
    'calculator'  => 'Calculator',
];