<?php 

return [
    'copyright' => 'Copyright',
    'rights-reserveed' => 'All rights reserved.',
    'lang-en' => 'English',
    'lang-id' => 'Indonesian',
    'or' => 'or',
    'email-sent' => 'Thank you! Your e-mail has been sent. Please wait for a reply from our staff members.',
];