<?php 

return [
    'title' => 'Contact Us',
    'factory' => 'Factory',
    'office-sby' => 'Surabaya Office',
    'office-jkt' => 'Jakarta Office',
    'form' => [
        'first-name' => 'First Name',
        'last-name' => 'Last Name',
        'phone' => 'Phone Number',
        'company' => 'Company',
        'email' => 'Email',
        'subject' => 'Subject/Title',
        'message' => 'Your Message',
        'agree' => 'I understand that I will be sharing this information with PT. Sumber Djaja Perkasa to inform me about their products & services.',
        'send' => 'Submit',
    ],
];