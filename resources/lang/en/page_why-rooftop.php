<?php 

return [
    'title' => 'Why Rooftop®',
    'benefit-1' => [
        'title' => 'BEST HEAT INSULATION',
        'subtitle' => 'REFLECT HEAT UP TO 71%',
        'description' => 'ROOFTOP® is made of a high performance uPVC (unplasticized polyvinyl chloride) resin with anti-UV and heat protection additives. The main material used, uPVC has a very low thermal conductivity, which means that it does not conduct heat as easily compared to other roofing profiles with different materials such as metal or asbestos. Our special anti UV and heat protection additives ensure that up to 71% of heat emitted by the sun is reflected; while the rest is absorbed and insulated by ROOFTOP®\'s double-wall structure.',
    ],
    'benefit-2' => [
        'title' => 'BEST NOISE INSULATION',
        'subtitle' => 'REDUCE NOISE UP TO 15 DB',
        'description' => 'The double wall structure and twin-wall air cavity that ROOFTOP® provides can reduce noise up to 15 db. This is particularly beneficial during rainy or monsoon season since it can significantly reduce raindrop noise.',
    ],
    'benefit-3' => [
        'title' => 'IMMUNE TO CORROSION',
        'subtitle' => 'PERFECT FOR TROPICAL WEATHER',
        'description' => 'ROOFTOP® main material, uPVC plastic is famously known to be a rust-proof material. This means that you can rest assure and do not have to worry about any corrosion that may occur since ROOFTOP® is immune to corrosion even in the harshest climate. This is especially beneficial in tropical countries where the humidity level is very high.',
    ],
    'benefit-4' => [
        'title' => 'DURABLE & RESILIENT',
        'subtitle' => 'SUPPORT WEIGHT UP TO 540 KG',
        'description' => 'ROOFTOP® durability and long product lifespan comes from its corrugated double-wall structure as well as the usage of high quality uPVC and additives. ROOFTOP® is very durable, has good mechanical strength properties and can support weight up to 540 kg/m2.',
    ],
    'benefit-5' => [
        'title' => 'TIME EFFICIENT',
        'subtitle' => 'QUICK & EASY INSTALLATION',
        'description' => 'Due to its sturdiness, workers can safely walk on ROOFTOP® sheets during installation process. This will save time since workers are not only limited to step on the purlin during installation. Furthermore, ROOFTOP®\'s special interlock system makes it very easy to connect and install each roofing sheets. Lastly, unlike other roofing products, ROOFTOP® only requires 4 SDS (self drilling screw) for every square meter, which not only saves time, but also money!',
    ],
    'benefit-6' => [
        'title' => 'COST-EFFECTIVE',
        'subtitle' => 'LOW MAINTENANCE COST',
        'description' => 'ROOFTOP® double wall corrugated roofing virtually does not require any maintenance. ROOFTOP® will never corrode, is very durable, and have excellent chemical resistant properties. Therefore you never have to worry about spending any additional money for reparation or maintenance again. On top of that, the 15 years warranty that ROOFTOP® provides ensure that you will have the best value for your roofing investment!',
    ],
    'benefit-7' => [
        'title' => 'EXTREMELY DURABLE',
        'subtitle' => 'EXCELLENT IMPACT & CHEMICAL RESISTANCE',
        'description' => 'Besides being immune to corrosion, uPVC is also notably known to be a material that has an excellent resistance against chemical substances. uPVC can resist various types of acids, bases and salts, which contributes to its immunity against rust. In addition to the chemical resistance, high quality impact modifiers additive inside ROOFTOP®’s composition ensure that it will not shatter easily during impact.',
    ],
    'benefit-8' => [
        'title' => 'LIGHT PERMEABILITY',
        'subtitle' => 'ENERGY CONSERVATION',
        'description' => 'One of our product variance, the semi-transparent ROOFTOP® roofing profile, offer as much as 20% light permeability. This means that during day time you can save money by taking advantage of the sun’s ray to illuminate the environment instead of wasting energy and money by turning on the lights.',
    ],
];