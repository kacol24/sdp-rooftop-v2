<?php 

return [
    'intro' => [
        'title' => 'The Pioneering uPVC Roofing Profile in Indonesia Since 2004',
        'text' => 'ROOFTOP® is made of high performance uPVC (unplasticized polyvinyl chloride) plastic and multitude of best grade additives that has been tested to comply with international standards. ROOFTOP® is carefuly designed to provide maximum comfort and longevity for your roofing investment.',
    ],
    'specifications' => [
        'title' => 'Specifications',
        'spec' => [
            'overall-width' => 'Overall Width',
            'effective-width' => 'Effective Width',
            'effective-width-overlap' => 'after overlap',
            'length' => 'Length',
            'material' => 'Material',
            'material-text' => 'High Performance uPVC with UV Protection',
            'colors' => 'Colors',
            'colors-text' => 'Light Blue<br>White<br>Custom Color*',
            'roof-pitch' => 'Roof Pitch',
            'purlin-spacing' => 'Purlin Spacing',
            'types' => 'Types',
            'types-text' => 'Opaque (Solid Color)<br>Semi Transparent (Transluscent)',
            'overall-thickness' => 'Overall Thickness',
            'weight' => 'Weight',
        ],
        'minimum-order' => 'Minimum order quantity applies',
        'recommended' => 'Recomendded',
    ],
    'performance' => [
        'title' => 'Performance',
        'subtitle' => 'Durable and Long Lasting',
        'text' => 'Due to its double wall corrugated construction as well as the high performance material used, ROOFTOP® is very durable and can withstand weight of up to 540 kg/m². Contrary to popular belief, the plastic that is used to produce ROOFTOP® is uPVC, which is a widely known plastic material that have an inherent fire resistant ability. Since uPVC is made of 50% chlorine (salt), ROOFTOP(R) will not catch fire easily, will not spread, and are self-extinguishing.',
        'withstand' => 'Withstand up to 540 kg/m²',
        'fire' => 'Will Not Spread Fire',
        'chemicals' => 'Excellent Resistance Against Household Chemicals',
    ],
    'interlock' => [
        'title' => 'Interlock System',
        'subtitle' => 'Revolutionary Leakproof Interlocking System',
        'text' => 'ROOFTOP®\'s interlock system ensures a precise installation and leaves no gaps between connection of each roof profiles. Combined with the innovative ROOFSEAL® screw fastener product, ROOFTOP® ensures that you will never have to worry about leakage and spending additional money for reparation or replacement ever again.',
    ],
    'title' => 'Product',
    'types' => [
        'title' => 'Types',
        'subtitle' => 'Different Types for Different Needs',
        'text' => 'ROOFTOP® comes in two types to cater to your roofing needs.',
        'opaque' => [
            'title' => 'Opaque',
            'text' => 'Opaque ROOFTOP® provides maximum protection against heat and sun\'s UV radiation. Opaque ROOFTOP® sheet will block all source of light.',
        ],
        'semi-tr' => [
            'title' => 'Semi Transparent (Transluscent)',
            'text' => 'Semi Transparent (Translucent) ROOFTOP® provides light permeability feature that allows up to 20% of light to go through, which can help to save energy during daytime since you can take advantage of the sun’s ray.',
        ],
    ],
    'accessories' => [
        'title' => 'Supporting Accessories',
        'top-ridge' => [
            'title' => 'Top Ridge',
            'text' => 'Excellent accessories for gable roof, adds extra protection against leakage as well as increasing the aesthetic values of your roof. ROOFTOP® Top Ridge is composed of the same material as the roof, which means it has the same durability and longevity.',
        ],
        'spec' => [
            'width' => 'Width',
            'length' => 'Length',
            'color-options' => 'Color Options',
            'color' => [
                'white' => 'White',
                'light-blue' => 'Light Blue',
            ],
            'effective' => 'effective',
            'diameter' => 'Diameter',
            'top' => 'Top',
            'bottom' => 'Bottom',
        ],
        'roofseal' => [
            'text' => 'Roofseal® is our innovative screw fastener product designed to replace traditional the rubber ring in self drilling screw (SDS) . Roofseal® is made of the same material as ROOFTOP® and thus it can withstand the sun’s heat and UV radiation without any worry of becoming brittle or cracking overtime.',
        ],
        'gutter' => [
            'title' => 'Gutter',
            'text' => 'ROOFTOP® Gutter is also made using the same uPVC material with high performance UV additives, which ensure that it will withstand extreme temperature with ease. The gutter is especially useful for carrying off rainwater and complement the overall look and performance of your roof.',
        ],
        'gutter-connector' => [
            'title' => 'Gutter Connector',
            'text' => 'ROOFTOP® Gutter Connector is also made of the same durable material as the gutter that functions as the connector between gutter to channel and redirect rainwater runoff. The design, construction and material used forms a leak-resistant joint between gutters.',
        ],
        'gutter-endcap' => [
            'title' => 'Gutter Endcap',
            'text' => 'ROOFTOP® Gutter End Caps are used to close the end of a gutter run. Featuring the same durable material used in the gutter, it ensures long product lifetime and less leakage risk.',
        ],
    ],
    'genuine' => [
        'title' => 'Genuine ROOFTOP®',
        'subtitle' => 'Imitated by Many, Surpassed by None',
        'text' => 'ROOFTOP® hold the original design patent since 2004 and is the most widely used brand for double wall corrugated uPVC roofing product in Indonesia. With countless brands that offer similar design now being sold on the market, ensure that you buy the genuine ROOFTOP®product. Ensure that:',
        'handle-weight' => 'Can it Handle Weight without Cracking / Breaking?',
        'precision' => 'Check the Design and the Precision of the Interlock System',
        'laser-printed-logo' => 'Check the Laser Printed Logo and Code On the Side of Every ROOFTOP®\'s Profile Sheet',
    ],
    'installation' => [
        'title' => 'Installation Procedure',
    ],
    'installation-1' => [
        'title' => 'Prepare The Purlin',
        'desc' => '- Ensure the purlin is in a straight and flat condition<br>- Draw a straight line before installing the roof',
    ],
    'installation-2' => [
        'title' => 'Roof Pitch',
        'desc' => '- The minimum roof pitch is 15° (degree)',
    ],
    'installation-3' => [
        'title' => 'Roof Installation',
    ],
    'installation-4' => [
        'title' => 'Installing SDS (Self Drilling Screw) and Roofseal®',
        'desc' => '- Install SDS by the following picture<small>(1 M² requires 4 SDS & 4 Roofseal®</small>',
    ],
    'installation-5' => [
        'title' => 'Overlap Length',
        'desc' => '- Overlap length between each roofing profiles (if present or neede) is 30 cm (assuming roof pitch is 15°)',
    ],
    'installation-6' => [
        'title' => 'Installing the Rain Gutter',
        'desc' => '- Install rain gutter and its accessories according to the picture',
    ],
    'installation-7' => [
        'title' => 'Application of Curved Roof',
        'desc' => '- ROOFTOP® application for arched roof can be done according to the picture',
    ],
];