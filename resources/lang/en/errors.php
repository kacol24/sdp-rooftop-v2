<?php

return [
    '404' => [
        'title' => '404 <small>Page Not Found.</small>',
        'text'  => 'Sorry, the page you are looking for could not be found. The link you followed may be broken, or the page may have been removed.',
    ],
    '500' => [
        'title' => '500 <small>Something Went Wrong.</small>',
        'text'  => 'Whoops! There was something wrong in processing your request.',
    ],
    '503' => [
        'title' => '503 <small>Service Unavailable</small>',
        'text'  => 'Be right back! We are doing some maintenance work to improve the site.',
    ],
];