<?php

return [
    'authorized-distributors' => 'Authorized distributors',
    'contact-us'              => 'Contact Us',
    'factory'                 => 'Factory',
    'our-company'             => 'Our Company',
    'product'                 => 'Product',
    'other-links'             => 'Other Links',
    'connect'                 => 'Connect',
    'rooftop-benefits'        => 'Rooftop Benefits',
    'rooftop-vs-competitor'   => 'Rooftop VS Competitor',
    'projects-portfolio'      => 'Projects Portfolio',
    'estimate-cost'           => 'Estimate Rooftop Cost',
];