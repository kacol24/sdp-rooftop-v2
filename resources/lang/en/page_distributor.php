<?php 

return [
    'title' => 'Authorized Distributors',
    'table' => [
        'name' => 'Name',
        'details' => 'Details',
        'location' => 'Location',
        'all-cities' => 'All Cities',
    ],
];