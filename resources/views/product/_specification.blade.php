<div class="container">
    <div class="row">
        <div class="col-12">
            <h2>@lang('page_product.specifications.title')</h2>
        </div>
        <div class="col-12">
            <div class="row mt-5">
                <div class="col-sm-6">
                    <dl class="row" data-aos="fade-right">
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.overall-width'):
                        </dt>
                        <dd class="col-sm-8">
                            820mm
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.effective-width'):
                        </dt>
                        <dd class="col-sm-8">
                            770 mm (@lang('page_product.specifications.spec.effective-width-overlap'))
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.length'):
                        </dt>
                        <dd class="col-sm-8">
                            6/8/10/12 m / Custom*
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.material'):
                        </dt>
                        <dd class="col-sm-8">
                            @lang('page_product.specifications.spec.material-text')
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.colors'):
                        </dt>
                        <dd class="col-sm-8">@lang('page_product.specifications.spec.colors-text')</dd>
                    </dl>
                </div>
                <div class="col-sm-6">
                    <dl class="row" data-aos="fade-left" data-aos-delay="300">
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.roof-pitch'):
                        </dt>
                        <dd class="col-sm-8">
                            15&deg;**
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.purlin-spacing'):
                        </dt>
                        <dd class="col-sm-8">
                            1.2M**
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.types'):
                        </dt>
                        <dd class="col-sm-8">@lang('page_product.specifications.spec.types-text')</dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.overall-thickness'):
                        </dt>
                        <dd class="col-sm-8">
                            10 mm
                        </dd>
                        <dt class="col-sm-4">
                            @lang('page_product.specifications.spec.weight'):
                        </dt>
                        <dd class="col-sm-8">
                            4.2 kg/m’ @lang('general.or') 5.45 kg/m²
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
    <p>
        * @lang('page_product.specifications.minimum-order')<br>
        ** @lang('page_product.specifications.recommended')
    </p>
    <figure class="figure" data-aos="fade-down">
        <img src="{{ cdn('images/product/spesifikasi-teknis-rooftop.png') }}" alt="spesifikasi rooftop"
             class="figure-img img-fluid">
    </figure>
</div>