<div class="row">
    <div class="col-12">
        <h2 data-aos="fade-up">
            @lang('page_product.types.title')
            <span class="subtitle mt-3" data-aos="fade-up"
                  data-aos-delay="200">@lang('page_product.types.subtitle')</span>
        </h2>
    </div>
    <div class="col-12 mt-5">
        <p>@lang('page_product.types.text')</p>
    </div>
    <div class="col-12">
        <div class="row mt-5">
            <div class="col-md-6">
                <figure class="figure" data-aos="zoom-in">
                    <img src="{{ cdn('images/product/rooftop-opaque.png') }}" alt="rooftop opaque"
                         class="figure-img img-fluid">
                    <figcaption class="figure-caption text-center">
                        <strong class="text-uppercase font-weight-bold">@lang('page_product.types.opaque.title')</strong>
                        <p>@lang('page_product.types.opaque.text')</p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-6">
                <figure class="figure" data-aos="zoom-in">
                    <img src="{{ cdn('images/product/rooftop-semiTR.png') }}" alt="rooftop semi transparent"
                         class="figure-img img-fluid">
                    <figcaption class="figure-caption text-center">
                        <strong class="text-uppercase font-weight-bold">@lang('page_product.types.semi-tr.title')</strong>
                        <p>@lang('page_product.types.semi-tr.text')</p>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
</div>