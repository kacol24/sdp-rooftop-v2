@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-product.jpg', 'class' => 'bg-c-b'])
        {{ trans('page_product.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.about-our-product') }}
    @endcomponent
@endsection

@section('content')
    <div id="product-page" class="mt-5 ovx-h">
        <section id="product" class="my-5 py-5 bg-light">
            @include('product._introduction')
        </section>
        <section id="specification" class="my-5 py-5">
            @include('product._specification')
        </section>
        <section id="performance" class="mt-5 py-5 bg-light">
            @include('product._performance')
        </section>
        <section id="interlock-system" class="mb-5 py-5">
            @include('product._interlock')
        </section>
        <section id="types" class="container my-5 py-5">
            @include('product._types')
        </section>
        <section id="supporting-accessories" class="my-5 py-5 bg-dark">
            @include('product._accessories')
        </section>
        <section id="genuine-rooftop" class="my-5 py-5 bg-light">
            @include('product._genuine')
        </section>
        <section id="installation" class="container mt-5 pt-5">
            @include('product._installation')
        </section>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => localized_route('why-rooftop.index')])
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.why-rooftop-better') }}
        @slot('button')
            {{ trans('cta.check-benefits') }}
        @endslot
    @endcomponent
@endsection