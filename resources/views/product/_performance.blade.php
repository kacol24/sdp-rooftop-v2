<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 data-aos="fade-up">
                @lang('page_product.performance.title')
                <span class="subtitle mt-3" data-aos="fade-up"
                      data-aos-delay="200">@lang('page_product.performance.subtitle')</span>
            </h2>
        </div>
        <div class="col-12 text-center my-5">
            <p>@lang('page_product.performance.text')</p>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="row">
                        <div class="col-6 col-md-4">
                            <figure class="figure text-center" data-aos="fade-left" data-aos-delay="300">
                                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     data-src="{{ cdn('images/product/tahan-beban.png') }}"
                                     alt="tahan beban"
                                     class="lazyload img-fluid w-25">
                                <figcaption class="figure-caption">
                                    @lang('page_product.performance.withstand')
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4">
                            <figure class="figure text-center" data-aos="fade-left" data-aos-delay="600">
                                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     data-src="{{ cdn('images/product/tahan-api.png') }}" alt="tahan api"
                                     class="lazyload img-fluid w-25">
                                <figcaption class="figure-caption">
                                    @lang('page_product.performance.fire')
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-6 col-md-4 mx-auto">
                            <figure class="figure text-center" data-aos="fade-left" data-aos-delay="900">
                                <img src="data:image/gif;base64,R0lGODlhAQABAIAAAP7//wAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=="
                                     data-src="{{ cdn('images/product/tahan-bahan-kimia.png') }}"
                                     alt="tahan bahan kimia"
                                     class="lazyload img-fluid w-25">
                                <figcaption class="figure-caption">
                                    @lang('page_product.performance.chemicals')
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 mt-5">
            <figure class="figure w-100">
                <img src="{{ cdn('images/product/rooftop-samping-2.png') }}" alt=""
                     class="figure-img w-100 img-fluid">
            </figure>
        </div>
    </div>
</div>