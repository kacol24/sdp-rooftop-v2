<div class="container">
    <h2 data-aos="fade-up">
        ROOFTOP®
        <span class="subtitle mt-3" data-aos="fade-up" data-aos-delay="200">
            {{ trans('page_product.intro.title') }}
        </span>
    </h2>
    <div class="row mt-5">
        <div class="col">
            <figure class="figure w-100" data-aos="fade" data-aos-offset="200">
                <img src="{{ cdn('images/product/rooftop-depan.png') }}" alt="rooftop tampak depan"
                     class="figure-img img-fluid w-100">
            </figure>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col">
            <p data-aos="fade-up">
                {{ trans('page_product.intro.text') }}
            </p>
        </div>
    </div>
</div>
<div class="container-fluid mt-5 pt-5">
    <div class="row">
        <figure class="figure w-100" data-aos="fade-in">
            <img src="{{ cdn('images/product/rooftop-samping.png') }}" alt="rooftop specifiation"
                 class="figure-img img-fluid w-100">
        </figure>
    </div>
</div>