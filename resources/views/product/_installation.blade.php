<div class="row">
    <div class="col">
        <h2 class="mb-5">
            @lang('page_product.installation.title')
        </h2>
        <div class="row">
            <div class="col">
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                1. @lang('page_product.installation-1.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---1.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-1.desc')
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                2. @lang('page_product.installation-2.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---2.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-2.desc')
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                3. @lang('page_product.installation-3.title')
                            </h5>
                        </div>
                        <div class="card-img-top">
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex align-items-center">
                                    <img class="img-fluid w-100"
                                         src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---3-1.png') }}"
                                         alt="Card image cap">
                                </div>
                                <div class="col-md-6">
                                    <img class="img-fluid w-100"
                                         src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---3-2.png') }}"
                                         alt="Card image cap">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                4. @lang('page_product.installation-4.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---4.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-4.desc')
                            </p>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                5. @lang('page_product.installation-5.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---5.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-5.desc')
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                6. @lang('page_product.installation-6.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---6.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-6.desc')
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                7. @lang('page_product.installation-7.title')
                            </h5>
                        </div>
                        <img class="card-img-top"
                             src="{{ cdn('images/product/'.current_locale().'-Image---Rooftop-Instalation---7.png') }}"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                @lang('page_product.installation-7.desc')
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card-deck">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">
                                Installation Video
                            </h5>
                        </div>
                        <div class="card-img-top">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item lazyload"
                                        data-src="https://www.youtube.com/embed/Q-rwyjsTQp0?rel=0&amp;showinfo=0&amp;modestbranding=1"
                                        frameborder="0" gesture="media" allow="encrypted-media"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
      $(function() {
        var installationSlideSelector = '#installation_slide';
        var installationCarouselSelector = '#installation_carousel';

        var $installationSlide = $(installationSlideSelector);
        var $installationCarousel = $(installationCarouselSelector);

        $installationCarousel.slick({
          slidesToShow: 5,
          slidesToScroll: 1,
          asNavFor: installationSlideSelector,
          dots: true,
          focusOnSelect: true,
          infinite: false,
          responsive: [
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                centerMode: true
              }
            }
          ]
        });
        $installationSlide.slick({
          adaptiveHeight: true,
          fade: true,
          asNavFor: installationCarouselSelector,
          infinite: false
        });

        $('#installation_slide_3').slick({
          dots: true,
          arrows: true
        });
      });
    </script>
@endpush