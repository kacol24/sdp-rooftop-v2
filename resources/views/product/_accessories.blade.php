<div class="container">
    <div class="row">
        <div class="col-12 mb-5">
            <h2>@lang('page_product.accessories.title')</h2>
        </div>
        <div class="col-12 accessories">
            <div class="row accessories__item">
                <div class="col-md-4" data-aos="fade-right">
                    <figure class="figure">
                        <img src="{{ cdn('images/product/top-ridge.png') }}"
                             alt="rooftop top ridge"
                             class="figure-img img-fluid">
                        <figcaption class="figure-caption text-center">
                            Top Ridge
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-8" data-aos="fade-left">
                    <p>@lang('page_product.accessories.top-ridge.text')</p>
                    <dl class="row justify-content-center">
                        <div class="col-md-auto">
                            <dt>350 mm</dt>
                            <dd>@lang('page_product.accessories.spec.width')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>820 mm (@lang('page_product.accessories.spec.effective') 770 mm)</dt>
                            <dd>Length</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>@lang('page_product.accessories.spec.color.white') & @lang('page_product.accessories.spec.color.light-blue')</dt>
                            <dd>@lang('page_product.accessories.spec.color-options')</dd>
                        </div>
                    </dl>
                </div>
            </div>
            <div class="row accessories__item">
                <div class="col-md-4" data-aos="fade-right">
                    <figure class="figure">
                        <img src="{{ cdn('images/product/rooftop-sds.png') }}"
                             alt="ROOFSEAL®"
                             class="figure-img img-fluid">
                        <figcaption class="figure-caption text-center">
                            ROOFSEAL®
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-8" data-aos="fade-left">
                    <p>@lang('page_product.accessories.roofseal.text')</p>
                    <dl class="row justify-content-center">
                        <div class="col-md-auto">
                            <dt>32 mm</dt>
                            <dd>@lang('page_product.accessories.spec.diameter')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>@lang('page_product.accessories.spec.color.white') & @lang('page_product.accessories.spec.color.light-blue')</dt>
                            <dd>@lang('page_product.accessories.spec.color-options')</dd>
                        </div>
                    </dl>
                </div>
            </div>
            <div class="row accessories__item">
                <div class="col-md-4" data-aos="fade-right">
                    <figure class="figure">
                        <img src="{{ cdn('images/product/Talang.png') }}"
                             alt="rooftop Gutter"
                             class="figure-img img-fluid">
                        <figcaption class="figure-caption text-center">
                            @lang('page_product.accessories.gutter.title')
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-8" data-aos="fade-left">
                    <p>@lang('page_product.accessories.gutter.text')</p>
                    <dl class="row justify-content-center">
                        <div class="col-md-auto">
                            <dt>434mm (@lang('page_product.accessories.spec.top')) / 380mm (@lang('page_product.accessories.spec.bottom'))</dt>
                            <dd>@lang('page_product.accessories.spec.width')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>6 m</dt>
                            <dd>@lang('page_product.accessories.spec.length')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>@lang('page_product.accessories.spec.color.white') & @lang('page_product.accessories.spec.color.light-blue')</dt>
                            <dd>@lang('page_product.accessories.spec.color-options')</dd>
                        </div>
                    </dl>
                </div>
            </div>
            <div class="row accessories__item">
                <div class="col-md-4" data-aos="fade-right">
                    <figure class="figure">
                        <img src="{{ cdn('images/product/sambungan-talang.png') }}"
                             alt="rooftop Gutter Connector"
                             class="figure-img img-fluid">
                        <figcaption class="figure-caption text-center">
                            @lang('page_product.accessories.gutter-connector.title')
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-8" data-aos="fade-left">
                    <p>@lang('page_product.accessories.gutter.text')</p>
                    <dl class="row justify-content-center">
                        <div class="col-md-auto">
                            <dt>453 mm (@lang('page_product.accessories.spec.top')) / 390 mm (@lang('page_product.accessories.spec.bottom'))</dt>
                            <dd>@lang('page_product.accessories.spec.width')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>@lang('page_product.accessories.spec.color.white') & @lang('page_product.accessories.spec.color.light-blue')</dt>
                            <dd>@lang('page_product.accessories.spec.color-options')</dd>
                        </div>
                    </dl>
                </div>
            </div>
            <div class="row accessories__item">
                <div class="col-md-4" data-aos="fade-right">
                    <figure class="figure">
                        <img src="{{ cdn('images/product/penutup-talang.png') }}"
                             alt="rooftop Gutter Endcap"
                             class="figure-img img-fluid">
                        <figcaption class="figure-caption text-center">
                            @lang('page_product.accessories.gutter-endcap.title')
                        </figcaption>
                    </figure>
                </div>
                <div class="col-md-8" data-aos="fade-left">
                    <p>@lang('page_product.accessories.gutter-endcap.text')</p>
                    <dl class="row justify-content-center">
                        <div class="col-md-auto">
                            <dt>460 mm (@lang('page_product.accessories.spec.top')) / 390 mm (@lang('page_product.accessories.spec.bottom'))</dt>
                            <dd>@lang('page_product.accessories.spec.width')</dd>
                        </div>
                        <div class="col-md-auto">
                            <dt>@lang('page_product.accessories.spec.color.white') & @lang('page_product.accessories.spec.color.light-blue')</dt>
                            <dd>@lang('page_product.accessories.spec.color-options')</dd>
                        </div>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>