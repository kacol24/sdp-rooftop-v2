<div class="container">
    <div class="row">
        <div class="col-12 mb-5">
            <h2 data-aos="fade-up">
                @lang('page_product.genuine.title')
                <span class="subtitle mt-3" data-aos="fade-up"
                      data-aos-delay="200">@lang('page_product.genuine.subtitle')</span>
            </h2>
            <p class="mt-5">@lang('page_product.genuine.text')</p>
        </div>
        <div class="col-sm-4">
            <figure class="figure" data-aos="fade-in">
                <img src="{{ cdn('images/product/printed-logo.png') }}" alt="printed logo"
                     class="figure-img img-fluid">
                <figcaption class="figure-caption">
                    <strong>
                        @lang('page_product.genuine.laser-printed-logo')
                    </strong>
                </figcaption>
            </figure>
        </div>
        <div class="col-sm-4">
            <figure class="figure" data-aos="fade-in" data-aos-delay="300">
                <img src="{{ cdn('images/product/precise-interlock.png') }}"
                     alt="precise interlock system"
                     class="figure-img img-fluid">
                <figcaption class="figure-caption">
                    <strong>
                        @lang('page_product.genuine.precision')
                    </strong>
                </figcaption>
            </figure>
        </div>
        <div class="col-sm-4">
            <figure class="figure" data-aos="fade-in" data-aos-delay="600">
                <img src="{{ cdn('images/product/strong.png') }}" alt="strong"
                     class="figure-img img-fluid">
                <figcaption class="figure-caption">
                    <strong>
                        @lang('page_product.genuine.handle-weight')
                    </strong>
                </figcaption>
            </figure>
        </div>
    </div>
</div>