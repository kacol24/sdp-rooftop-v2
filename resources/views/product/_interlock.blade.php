<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 data-aos="fade-up">
                @lang('page_product.interlock.title')
                <span class="subtitle mt-3" data-aos="fade-up"
                      data-aos-delay="200">@lang('page_product.interlock.subtitle')</span>
            </h2>
        </div>
        <div class="col-12 mt-5">
            <p>@lang('page_product.interlock.text')</p>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <figure class="figure w-100">
            <img src="{{ cdn('images/product/roof-seal.png') }}" alt="rooftop roof seal"
                 class="figure-img img-fluid w-100">
        </figure>
    </div>
</div>