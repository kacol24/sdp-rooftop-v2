@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-pricelist.jpg', 'class' => 'bg-c-b'])
        {{ trans('page_pricelist.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.helpful-downloads') }}
    @endcomponent
@endsection

@section('content')
    <div id="pricelist-page" class="container">
        <div class="row">
            <div class="col">
                <ul class="list-unstyled pricelist-list">
                    @foreach($downloads as $download)
                        <li>
                            <a href="{{ cdn($download->path) }}">
                                <i class="fa fa-file-pdf-o"></i> {{ $download->name }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => 'mailto:marketing@rooftop.co.id'])
        @slot('icon')
            <i class="cta__icon cta__icon--users"></i>
        @endslot
        {{ trans('cta.distributors-wanted') }}
        @slot('button')
            {{ trans('cta.send-us-email') }}
        @endslot
    @endcomponent
@endsection