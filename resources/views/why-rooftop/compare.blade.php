@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-why_rooftop.jpg', 'class' => 'bg-c-t'])
        MENGAPA ROOFTOP®
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        MENGAPA ANDA HARUS MEMILIH ROOFTOP
    @endcomponent
@endsection

@section('content')
    <div id="why-rooftop-page" class="container">
        @include('why-rooftop._subnav')
        <div class="row">
            <div class="col-12 col-md-6">
                <figure class="figure">
                    <img src="{{ cdn('images/rooftopvsmetal.png') }}" alt="rooftop vs metal"
                         class="figure-img img-fluid">
                </figure>
            </div>
            <div class="w-100"></div>
            <div class="col-12">
                <h3 class="text-primary">
                    ROOFTOP® VS. ATAP GELOMBANG LOGAM
                </h3>
                <p>
                    Ketika memilih atap, atap gelombang metal/logam adalah salah satu pilihan yang paling popular
                    dikarenakan oleh harganya yang relatif terjangkau. Meskipun atap gelombang logam tipe apapun (besi,
                    baja galvanis, aluminium, dan lain – lain) terlihat murah dan terjangkau, atap tersebut membutuhkan
                    tambahan produk lain seperti insulasi panas dan waterproofing untuk bisa berfungsi secara maksimal.
                    Pada akhirnya, semua hal ini akan memakan lebih banyak waktu dan uang dikarenakan perawatan dan
                    penambahan produk – produk lain (insulasi panas dan suara, dll.). Atap Dingin uPVC Rooftop® didesain
                    untuk menjadi solusi bagi semua masalah tersebut:
                </p>
                <ul>
                    <li>
                        Tidak seperti atap logam yang memerlukan tambahan insulasi panas, struktur dinding ganda dan
                        rongga udara Atap Dingin ROOFTOP® memberikan pertahanan maksimum terhadap panas dan berperforma
                        baik atau bahkan lebih baik dibandingkan dengan atap logam yang telah diinsulasi.
                    </li>
                    <li>
                        Tidak seperti atap logam yang berisik dan memerlukan tambahan insulasi suara, material PVC dan
                        struktur dinding ganda Atap Dingin ROOFTOP® dapat mengurangi suara hingga 15db sehingga anda
                        dapat melanjutkan aktifitas anda tanpa terganggu oleh suara berisik yang timbul saat hujan atau
                        keadaan berangin.
                    </li>
                    <li>
                        Tidak seperti kebanyakan atap logam yang dapat berkarat (terutama di negara tropis di mana
                        tinggat kelembapan sangat tinggi), material utama Atap Dingin ROOFTOP®, uPVC, memastikan bahwa
                        atap ROOFTOP® tidak akan pernah bisa berkarat meskipun di dalam kondisi esktrim.
                    </li>
                    <li>
                        Tidak seperti beberapa tipe atap logam yang tidak kokoh dan tidak aman untuk diinjak, material
                        dan struktur mekanis dari atap Atap Dingin ROOFTOP® membuat atap Rooftop® aman untuk diinjak dan
                        bisa menahan beban hingga 540 kg, sehingga perawatan dan proses instalasi menjadi lebih cepat
                        dan aman.
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-6">
                <figure class="figure">
                    <img src="{{ cdn('images/rooftopvpolycarbonate.png') }}" alt="rooftop vs polycarbonate"
                         class="figure-img img-fluid">
                </figure>
            </div>
            <div class="col-12">
                <h3 class="text-primary">
                    ROOFTOP® VS. ATAP POLYCARBONATE
                </h3>
                <p>
                    Sama halnya dengan atap logam, atap berbahan polycarbonate juga merupakan salah satu pilihan paling
                    populer, biasanya dikarenakan oleh kekuatan dan kemampuan atap polycarbonate untuk menembuskan
                    cahaya. Akan tetapi atap polycarbonate rentan terhadap bahan kimia dan sering kali menjadi getas
                    setelah terkena panas dan radiasi UV dari sinar matahari setelah beberapa waktu berlalu. Selain itu
                    atap polycarbonate juga memerlukan perawatan yang lebih banyak karena permukaan yang berongga rentan
                    menarik jamur / lumut yang sulit untuk dibersihkan dan juga warna atap lebih cepat pudar jika
                    terkena sinar matahari secara terus menerus. Atap Dingin uPVC ROOFTOP® didesain untuk menjadi solusi
                    bagi semua masalah tersebut:
                </p>
                <ul>
                    <li>
                        Tidak seperti atap polycarbonate yang sensitif terhadap cairan pembersih abrasif, produk
                        pembersih alkalin dan larutan organik lainnya, Atap Dingin ROOFTOP® memberikan pertahanan
                        maksimum terhadap berbagai jenis bahan kimia.
                    </li>
                    <li>
                        Tidak seperti atap polycarbonate yang menjadi getas (mengeras) dan cenderung mengalami keretakan
                        setelah terkena panas dan sinar UV dari matahari, Atap Dingin ROOFTOP® tahan terhadap kondisi
                        cuaca yang ekstrim tanpa menimbulkan deformasi.
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => '#'])
        @slot('icon')
            <i class="cta__icon cta__icon--question"></i>
        @endslot
        MEMILIKI PERTANYAAN YANG BELUM TERJAWAB?
        @slot('button')
            KUNJUNGI HALAMAN FAQS
        @endslot
    @endcomponent
@endsection