<div class="row justify-content-between text-center mb-5">
    <div class="col-md-4 mb-md-0 mb-3">
        <a href="{{ route('why-rooftop.index') }}"
           class="btn btn-primary {{ active_class(if_route('why-rooftop.index')) }}">
            MANFAAT ROOFTOP®
        </a>
    </div>
    <div class="col-md-4">
        <a href="{{ route('why-rooftop.compare') }}"
           class="btn btn-primary {{ active_class(if_route('why-rooftop.compare')) }}">
            ROOFTOP® VS ATAP LAIN
        </a>
    </div>
</div>