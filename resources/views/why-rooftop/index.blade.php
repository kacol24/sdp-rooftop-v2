@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-why_rooftop.jpg', 'class' => 'bg-c-t'])
        {{ trans('page_why-rooftop.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.rooftop-benefits') }}
    @endcomponent
@endsection

@section('content')
    <div id="why-rooftop-page" class="container">
        @include('why-rooftop._subnav')
        <div class="row">
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/01-Tolak-panas.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-1.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-1.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-1.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/02-Kedap-suara.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-2.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-2.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-2.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/03-Tidak-akan-berkarat.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-3.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-3.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-3.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/04-kuat-menahan-beban.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-4.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-4.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-4.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/05-Instalasi-cepat.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-5.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-5.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-5.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/06-Ekonomis-perawatan-mudah.jpg') }}" alt="tolak panas" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-6.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-6.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-6.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/07-Tahan-bahan-kimia.jpg') }}" alt="tolak panas"
                         class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-7.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-7.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-7.description')</p>
            </div>
            <div class="col-md-4">
                <figure class="figure">
                    <img src="{{ cdn('images/08-tembus-cahaya-hemat-energi.jpg') }}" alt="tolak panas"
                         class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                        <p class="lead">
                            @lang('page_why-rooftop.benefit-8.title')
                        </p>
                        <p>
                            @lang('page_why-rooftop.benefit-8.subtitle')
                        </p>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-8">
                <p>@lang('page_why-rooftop.benefit-8.description')</p>
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => '#'])
        @slot('icon')
            <i class="cta__icon cta__icon--question"></i>
        @endslot
        {{ trans('cta.unanswered-question') }}
        @slot('button')
            {{ trans('cta.check-faq') }}
        @endslot
    @endcomponent
@endsection