@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-faq.jpg'])
        {{ trans('page_faq.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.find-answers') }}
    @endcomponent
@endsection

@section('content')
    <div id="faq-page" class="container">
        <section class="row">
            <div class="col-12 mb-5">
                <input type="search" class="search form-control" placeholder="{{ trans('page_faq.what-is-your-question') }}">
            </div>
            <div class="col-12">
                <ul class="faq list-unstyled">
                    @foreach($faqs as $index => $faq)
                        <li>
                            <div data-target="#answer-{{ $index }}" class="faq__question" data-toggle="collapse"
                                 aria-expanded="false"
                                 aria-controls="question-{{ $index }}">
                                <div class="faq__question-numbers">{{ $index + 1 }}.</div>
                                <div class="faq__question-container">{!! $faq->question[current_locale()] !!}</div>
                            </div>
                            <div class="collapse faq__answer" id="answer-{{ $index }}">
                                {!! $faq->answer[current_locale()] !!}
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => localized_route('contact.index')])
        @slot('icon')
            <i class="cta__icon cta__icon--question"></i>
        @endslot
        {{ trans('cta.still-need-help') }}
        @slot('button')
            {{ trans('cta.contact-us') }}
        @endslot
    @endcomponent
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/list.js@1/dist/list.min.js"></script>
    <script>
      $(function() {
        var faqList = new List('faq-page', {
          valueNames: ['faq__question'],
          listClass: 'faq'
        });
      });
    </script>
@endpush