@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-our-company.jpg'])
        {!! trans('page_our-company.title') !!}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.leading-innovative') }}
    @endcomponent
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center order-md-12">
                <figure class="figure w-100">
                    <img src="{{ cdn('images/iso.png') }}" alt="logo ISO" class="img-fluid">
                </figure>
                <figure class="figure w-100">
                    <img src="{{ cdn('images/pertama.png') }}" alt="logo pertama di Indonesia" class="img-fluid">
                </figure>
                <figure class="figure w-100">
                    <img src="{{ cdn('images/gbci.jpg') }}" alt="logo GBCI & 100% Indonesia" class="img-fluid">
                </figure>
            </div>
            <div class="col-md-8 text-justify order-md-1">
                {!! trans('page_our-company.article') !!}
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => 'mailto:marketing@rooftop.co.id'])
        @slot('icon')
            <i class="cta__icon cta__icon--users"></i>
        @endslot
        {{ trans('cta.distributors-wanted') }}
        @slot('button')
            {{ trans('cta.send-us-email') }}
        @endslot
    @endcomponent
@endsection