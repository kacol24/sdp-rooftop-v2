<div class="modal modal--fullscreen fade" tabindex="-1" role="dialog" id="project-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            {{--<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>--}}
                            <ul id="category-slider"
                                class="list-unstyled project-slide" m-if="images">
                                <li m-for="image in images">
                                    <img src="data:image/gif;base64,R0lGODlh6APuAoAAAP///wAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwNjcgNzkuMTU3NzQ3LCAyMDE1LzAzLzMwLTIzOjQwOjQyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowNjVCQjAwM0IxNTgxMUU3OTUxOEY1QzFFNUYyMkQ0RiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowNjVCQjAwNEIxNTgxMUU3OTUxOEY1QzFFNUYyMkQ0RiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjA2NUJCMDAxQjE1ODExRTc5NTE4RjVDMUU1RjIyRDRGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjA2NUJCMDAyQjE1ODExRTc5NTE4RjVDMUU1RjIyRDRGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAAAsAAAAAOgD7gJAAv+Ej6nL7Q+jnLTai7PevPsPhuJIluaJpurKtu4Lx/JM1/aN5/rO9/4PDAqHxKLxiEwql8ym8wmNSqfUqvWKzWq33K73Cw6Lx+Sy+YxOq9fstvsNj8vn9Lr9js/r9/y+/w8YKDhIWGh4iJiouMjY6PgIGSk5SVlpeYmZqbnJ2en5CRoqOkpaanqKmqq6ytrq+gobKztLW2t7i5uru8vb6/sLHCw8TFxsfIycrLzM3Oz8DB0tPU1dbX2Nna29zd3t/Q0eLj5OXm5+jp6uvs7e7v4OHy8/T19vf4+fr7/P3+//DzCgwIEECxo8iDChwoUMGzp8CDGixIkUK1q8iDGjxo3/HDt6/AgypMiRJEuaPIkypcqVLFu6fAkzpsyZNGvavIkzp86dPHv6/Ak0qNChRIsaPYo0qdKlTJs6fQo1qtSpVKtavYo1q9atXLt6/Qo2rNixZMuaPYs2rdq1bNu6fQs3rty5dOvavYs3r969fPv6/Qs4sODBhAsbPow4seLFjBs7fgw5suTJlCtbvow5s+bNnDt7/gw6tOjRpEubPo06terVrFu7fg07tuzZtGvbvo07t+7dvHv7/g08uPDhxIsbP448ufLlzJs7fw49uvTp1Ktbv449u/bt3Lt7/w4+vPjx5MubP48+vfr17Nu7fw8/vvz59Ovbv48/v/79/Pv7//8PYIACDkhggQYeiGCCCi7IYIMOPghhhBJOSGGFFl6IYYYabshhhx5+CGKIIo5IYokmnohiiiquyGKLLr4IY4wyzkhjjTbeiGOOOu7IY48+/ghkkEIOSWSRRh6JZJJKLslkk04+CWWUUk5JZZVWXollllpuyWWXXn4JZphijklmmWaeiWaaaq7JZptuvglnnHLOSWeddt6JZ5567slnn37+CWiggg5KaKGGHopoooouymijjj4KaaSSTkpppZZeimmmmm7KaaeefgpqqKKOSmqppp6Kaqqqrspqq66+Cmusss5Ka6223oprrrruymuvvv4KbLDCDktsscYei2yyyv8uy2yzzj4LbbTSTktttdZei2222m7LbbfefgtuuOKOS2655p6Lbrrqrstuu+6+C2+88s5Lb7323otvvvruy2+//v4LcMACD0xwwQYfjHDCCi/McMMOPwxxxBJPTHHFFl+MccYab8xxxx5/DHLIIo9Mcskmn4xyyiqvzHLLLr8Mc8wyz0xzzTbfjHPOOu/Mc88+/wx00EIPTXTRRh+NdNJKL810004/DXXUUk9NddVWX4111lpvzXXXXn8Ndthij0122WafjXbaaq/Ndttuvw133HLPTXfddt+Nd9567813337/DXjggg9OeOGGH4544oovznjjjj8OeeSST0555Zb/X4555ppvznnnnn8Oeuiij0566aafjnrqqq/Oeuuuvw577LLPTnvttt+Oe+667857777/Dnzwwg9PfPHGH4988sovz3zzzj8PffTST0999dZfj3322m/Pfffefw9++OKPT3755p+Pfvrqr89+++6/D3/88s9Pf/32349//vrvz3///v8PwAAKcIAELKABD4jABCpwgQxsoAMfCMEISnCCFKygBS+IwQxqcIMc7KAHPwjCEIpwhCQsoQlPiMIUqnCFLGyhC18IwxjKcIY0rKENb4jDHOpwhzzsoQ9/CMQgCnGIRCyiEY+IxCQqcYlMbKITnwjFKEpxilSsohWviMUsompxi1zsohe/CMYwinGMZCyjGc+IxjSqcY1sbKMb3wjHOMpxjnSsox3viMc86nGPfOyjH/8IyEAKcpCELKQhD4nIRCpykYxspCMfCclISnKSlKykJS+JyUxqcpOc7KQnPwnKUIpylKQspSlPicpUqnKVrGylK18Jy1jKcpa0rKUtb4nLXOpyl7zspS9/CcxgCnOYxCymMY+JzGQqc5nMhEkBAAA7"
                                         data-lazy="@{{ image.full }}" class="img-fluid w-100">
                                    {{--<img src="@{{ image.full }}" class="img-fluid w-100">--}}
                                </li>
                            </ul>
                            <ul id="category-carousel"
                                class="list-unstyled project-carousel hidden-xs" m-if="images">
                                <li m-for="image in images">
                                    <img src="data:image/gif;base64,R0lGODlh6APuAoAAAP///wAAACH/C1hNUCBEYXRhWE1QPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMwNjcgNzkuMTU3NzQ3LCAyMDE1LzAzLzMwLTIzOjQwOjQyICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoTWFjaW50b3NoKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDowNjVCQjAwM0IxNTgxMUU3OTUxOEY1QzFFNUYyMkQ0RiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDowNjVCQjAwNEIxNTgxMUU3OTUxOEY1QzFFNUYyMkQ0RiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjA2NUJCMDAxQjE1ODExRTc5NTE4RjVDMUU1RjIyRDRGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjA2NUJCMDAyQjE1ODExRTc5NTE4RjVDMUU1RjIyRDRGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+Af/+/fz7+vn49/b19PPy8fDv7u3s6+rp6Ofm5eTj4uHg397d3Nva2djX1tXU09LR0M/OzczLysnIx8bFxMPCwcC/vr28u7q5uLe2tbSzsrGwr66trKuqqainpqWko6KhoJ+enZybmpmYl5aVlJOSkZCPjo2Mi4qJiIeGhYSDgoGAf359fHt6eXh3dnV0c3JxcG9ubWxramloZ2ZlZGNiYWBfXl1cW1pZWFdWVVRTUlFQT05NTEtKSUhHRkVEQ0JBQD8+PTw7Ojk4NzY1NDMyMTAvLi0sKyopKCcmJSQjIiEgHx4dHBsaGRgXFhUUExIREA8ODQwLCgkIBwYFBAMCAQAAIfkEAQAAAAAsAAAAAOgD7gJAAv+Ej6nL7Q+jnLTai7PevPsPhuJIluaJpurKtu4Lx/JM1/aN5/rO9/4PDAqHxKLxiEwql8ym8wmNSqfUqvWKzWq33K73Cw6Lx+Sy+YxOq9fstvsNj8vn9Lr9js/r9/y+/w8YKDhIWGh4iJiouMjY6PgIGSk5SVlpeYmZqbnJ2en5CRoqOkpaanqKmqq6ytrq+gobKztLW2t7i5uru8vb6/sLHCw8TFxsfIycrLzM3Oz8DB0tPU1dbX2Nna29zd3t/Q0eLj5OXm5+jp6uvs7e7v4OHy8/T19vf4+fr7/P3+//DzCgwIEECxo8iDChwoUMGzp8CDGixIkUK1q8iDGjxo3/HDt6/AgypMiRJEuaPIkypcqVLFu6fAkzpsyZNGvavIkzp86dPHv6/Ak0qNChRIsaPYo0qdKlTJs6fQo1qtSpVKtavYo1q9atXLt6/Qo2rNixZMuaPYs2rdq1bNu6fQs3rty5dOvavYs3r969fPv6/Qs4sODBhAsbPow4seLFjBs7fgw5suTJlCtbvow5s+bNnDt7/gw6tOjRpEubPo06terVrFu7fg07tuzZtGvbvo07t+7dvHv7/g08uPDhxIsbP448ufLlzJs7fw49uvTp1Ktbv449u/bt3Lt7/w4+vPjx5MubP48+vfr17Nu7fw8/vvz59Ovbv48/v/79/Pv7//8PYIACDkhggQYeiGCCCi7IYIMOPghhhBJOSGGFFl6IYYYabshhhx5+CGKIIo5IYokmnohiiiquyGKLLr4IY4wyzkhjjTbeiGOOOu7IY48+/ghkkEIOSWSRRh6JZJJKLslkk04+CWWUUk5JZZVWXollllpuyWWXXn4JZphijklmmWaeiWaaaq7JZptuvglnnHLOSWeddt6JZ5567slnn37+CWiggg5KaKGGHopoooouymijjj4KaaSSTkpppZZeimmmmm7KaaeefgpqqKKOSmqppp6Kaqqqrspqq66+Cmusss5Ka6223oprrrruymuvvv4KbLDCDktsscYei2yyyv8uy2yzzj4LbbTSTktttdZei2222m7LbbfefgtuuOKOS2655p6Lbrrqrstuu+6+C2+88s5Lb7323otvvvruy2+//v4LcMACD0xwwQYfjHDCCi/McMMOPwxxxBJPTHHFFl+MccYab8xxxx5/DHLIIo9Mcskmn4xyyiqvzHLLLr8Mc8wyz0xzzTbfjHPOOu/Mc88+/wx00EIPTXTRRh+NdNJKL810004/DXXUUk9NddVWX4111lpvzXXXXn8Ndthij0122WafjXbaaq/Ndttuvw133HLPTXfddt+Nd9567813337/DXjggg9OeOGGH4544oovznjjjj8OeeSST0555Zb/X4555ppvznnnnn8Oeuiij0566aafjnrqqq/Oeuuuvw577LLPTnvttt+Oe+667857777/Dnzwwg9PfPHGH4988sovz3zzzj8PffTST0999dZfj3322m/Pfffefw9++OKPT3755p+Pfvrqr89+++6/D3/88s9Pf/32349//vrvz3///v8PwAAKcIAELKABD4jABCpwgQxsoAMfCMEISnCCFKygBS+IwQxqcIMc7KAHPwjCEIpwhCQsoQlPiMIUqnCFLGyhC18IwxjKcIY0rKENb4jDHOpwhzzsoQ9/CMQgCnGIRCyiEY+IxCQqcYlMbKITnwjFKEpxilSsohWviMUsompxi1zsohe/CMYwinGMZCyjGc+IxjSqcY1sbKMb3wjHOMpxjnSsox3viMc86nGPfOyjH/8IyEAKcpCELKQhD4nIRCpykYxspCMfCclISnKSlKykJS+JyUxqcpOc7KQnPwnKUIpylKQspSlPicpUqnKVrGylK18Jy1jKcpa0rKUtb4nLXOpyl7zspS9/CcxgCnOYxCymMY+JzGQqc5nMhEkBAAA7"
                                         data-lazy="@{{ image.full }}" class="img-fluid w-100">
                                    {{--<img src="@{{ image.full }}" class="img-fluid w-100">--}}
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <div class="project-description">
                                <h4 class="project-title">
                                    @{{ title }}
                                </h4>
                                <p class="text-justify" m-if="description">
                                    @{{ description }}
                                </p>
                                <table class="table">
                                    <tbody>
                                    <tr m-if="location">
                                        <th>Location</th>
                                        <td>@{{ location }}</td>
                                    </tr>
                                    <tr m-if="distributor">
                                        <th>Distributor</th>
                                        <td>@{{ distributor }}</td>
                                    </tr>
                                    <tr m-if="date">
                                        <th>Completed</th>
                                        <td>@{{ date }}</td>
                                    </tr>
                                    <tr m-if="category">
                                        <th>Category</th>
                                        <td>@{{ category }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="project-description-skeleton" m-if="loading">
                                <div class="project-description-item">
                                    <div class="animated-background">
                                        <div class="mask mask__title"></div>
                                        <div class="mask mask__separator"></div>
                                        <div class="mask mask__description-1"></div>
                                        <div class="mask mask__separator--mini"></div>
                                        <div class="mask mask__description-2"></div>
                                        <div class="mask mask__separator--mini"></div>
                                        <div class="mask mask__description-3"></div>
                                        <div class="mask mask__separator"></div>
                                        <div class="mask mask__location"></div>
                                        <div class="mask mask__separator"></div>
                                        <div class="mask mask__distributor"></div>
                                        <div class="mask mask__separator"></div>
                                        <div class="mask mask__completed"></div>
                                        <div class="mask mask__separator"></div>
                                        <div class="mask mask__category"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="https://cdn.jsdelivr.net/combine/npm/moonjs@0.11.0,npm/axios@0.17.0/dist/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"
            integrity="sha256-4Cr335oZDYg4Di3OwgUOyqSTri0jUm2+7Gf2kH3zp1I=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script>
      var $modal = $('#project-modal');
      var $projectSlide = $('.project-slide');
      var $projectCarousel = $('.project-carousel');

      $projectSlide.on('init', function() {
        console.log('slick init');
      });

      $modal.on('hidden.bs.modal', function() {
        $projectCarousel.slick('unslick');
        $projectSlide.slick('unslick');

        RooftopProject.set('title', '');
        RooftopProject.set('description', '');
        RooftopProject.set('distributor', '');
        RooftopProject.set('location', '');
        RooftopProject.set('date', '');
        RooftopProject.set('images', []);
        RooftopProject.set('category', '');
        RooftopProject.set('loading', true);
      });

      $modal.on('shown.bs.modal', function() {

        $projectSlide.slick({
          adaptiveHeight: true,
          asNavFor: '.project-carousel',
          infinite: false,
          arrows: false,
          lazyLoad: 'progressive',
          slidesToShow: 1,
          slidesToScroll: 1,
          fade: true,
          responsive: [
            {
              breakpoint: 480,
              settings: {
                fade: false,
                lazyload: 'ondemand',
                arrows: true,
                dots: true
              }
            }
          ]
        });
        $projectCarousel.slick({
          adaptiveHeight: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          asNavFor: '.project-slide',
          dots: true,
          focusOnSelect: true,
          infinite: false,
          lazyLoad: 'progressive',
          responsive: [
            {
              breakpoint: 991,
              settings: {
                dots: false,
                arrows: false
              }
            },
            {
              breakpoint: 480,
              settings: 'unslick'
            }
          ]
        });

      });

      var RooftopProject = new Moon({
        el: '#project-modal',

        data: {
          locale: '',
          title: '',
          description: '',
          distributor: '',
          location: '',
          date: '',
          images: [],
          category: '',
          loading: true
        }
      });

      $(document).on('click', '.project-item__button', function(e) {
        e.preventDefault();
        var loadingText = '<i class="fa fa-refresh fa-spin"></i> loading..';

        var button = this;
        var url = button.href;
        var originalText = button.innerHTML;
        button.innerHTML = loadingText;
        $(button).attr('disabled');
        $(button).addClass('disabled');

        axios.get(url).then(function(response) {
          result = response.data.data;
          RooftopProject.set('title', result.title);
          RooftopProject.set('description', result.description);
          RooftopProject.set('distributor', result.distributor);
          RooftopProject.set('location', result.location);
          RooftopProject.set('date', result.date);
          RooftopProject.set('images', result.images);
          RooftopProject.set('category', result.category);
          RooftopProject.set('loading', false);

          $modal.modal();
          button.innerHTML = originalText;
          $(button).removeAttr('disabled');
          $(button).removeClass('disabled');
        });
      });
    </script>
@endpush