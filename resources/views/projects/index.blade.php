@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-project.jpg'])
        {{ trans('page_projects.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.finished-projects') }}
    @endcomponent
@endsection

@section('content')
    <div id="project-page" class="container">
        <div class="row">
            <div class="col">
                <div id="filter-buttons" class="filter-button-group text-center">
                    <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>
                    <ul class="list-inline">
                        @foreach($projectCategories as $projectCategory)
                            <li class="list-inline-item">
                                <a href="#"
                                   data-filter=".{{ $projectCategory->slug }}"
                                   class="btn {{ $loop->first ? 'btn-primary disabled' : 'btn-outline-primary' }}">
                                    {{ is_array($projectCategory->name) ? $projectCategory->name[LaravelLocalization::getCurrentLocale()] : $projectCategory->name }}
                                    ({{ $projectCategory->projects->count() }})
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div id="projects" class="row">
            <div class="grid-sizer col-12 col-sm-6 col-md-4"></div>
            @foreach($projectCategories as $projectCategory)
                @foreach($projectCategory->projects->sortByDesc(function($value){return (int)$value->date;}) as $project)
                    <div class="col-12 col-sm-6 col-md-4 project-item project-item--overlay-full {{ $projectCategory->slug }}">
                        <div class="project-item__wrapper">
                            <div class="project-item__image">
                                @if($featuredImage = $project->featured_image)
                                    @if(is_string($featuredImage))
                                        <img src="{{ Croppa::url('images/manipulated/uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $featuredImage, 250, 175, ['quality' => 30, 'filters' => ['blur']]) }}"
                                             data-src="{{ cdn('uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $featuredImage) }}"
                                             class="img-fluid lazyload blur-up"
                                             width="500" height="350"
                                             alt="{{ $project->title or $projectCategory->name }}">
                                    @else
                                        <img src="{{ cdn('uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $project->featured_image->thumb_lq) }}"
                                             data-src="{{ cdn('uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $project->featured_image->thumb) }}"
                                             class="img-fluid lazyload blur-up"
                                             alt="{{ $project->title or $projectCategory->name }}">
                                    @endif
                                @else
                                    @if(is_string($project->images->first()))
                                        <img src="{{ cdn('uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $project->images->first()) }}"
                                             class="img-fluid"
                                             alt="{{ $project->title or $projectCategory->name }}">
                                    @else
                                        <img src="{{ cdn('uploads/projects/'.$projectCategory->slug.'/'.$project->project_folder.'/'. $project->images->first()->thumb) }}"
                                             class="img-fluid"
                                             alt="{{ $project->title or $projectCategory->name }}">
                                    @endif
                                @endif
                            </div>
                            <div class="project-item__overlay">
                                <div class="project-item__content">
                                    <div class="project-item__title h4">{{ $project->title }}</div>
                                    @if($project->date)
                                        <div class="project-item__subtitle h6">Completed {{ $project->date }}</div>
                                    @endif
                                    <a href="{{ route('project.show', $project->project_folder) }}"
                                       class="btn btn-primary project-item__button">more info</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => localized_route('distributor.index')])
        @slot('icon')
            <i class="cta__icon cta__icon--cube"></i>
        @endslot
        {{ trans('cta.interested-buying') }}
        @slot('button')
            {{ trans('cta.locate-store-nearby') }}
        @endslot
    @endcomponent
    @include('projects._modal')
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/combine/npm/isotope-layout@3/dist/isotope.pkgd.min.js,npm/imagesloaded@4"></script>
    <script>
      $(document).ready(function() {
        // cache the dom
        var $filterButtonGroup = $('#filter-buttons');
        var $overlay = $filterButtonGroup.find('.overlay');
        var $projects = $('#projects').isotope({
          itemSelector: '.project-item',
          percentPosition: true,
          filter: '.{{ $projectCategories->first()->slug }}'
        });
        var $items = $projects.find('.project-item');
        $projects.addClass('is-showing-items').isotope('revealItemElements', $items);
        $projects.isotope({filter: '.{{ $projectCategories->first()->slug }}'});

        $projects.on('layoutComplete', function() {
          $overlay.hide();
        });

        $filterButtonGroup.on('click', 'a', function(e) {
          e.preventDefault();
          $overlay.show();

          var $this = $(this);
          var filterValue = $this.data('filter');
          $projects.isotope({filter: filterValue});
          $filterButtonGroup.find('a').
              removeClass('btn-primary btn-outline-primary disabled').
              addClass('btn-outline-primary');
          $this.addClass('btn-primary disabled').removeClass('btn-outline-primary');
        });
      });
    </script>
@endpush