@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-faq.jpg'])
        {{ trans('page_calculator.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.cost-estimation') }}
    @endcomponent
@endsection

@section('content')
    <div id="calculator-page" class="container">
        <section class="row">
            <div class="col-md-6 order-12 order-md-1">
                <form>
                    <fieldset m-literal:disabled="loading">
                        <div class="overlay" m-if="loading">
                            <i class="fa fa-refresh fa-spin fa-2x fa-fw"></i>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="rooftype_gable" name="roof_type"
                                       class="custom-control-input" value="gable" m-model="form.roof_type">
                                <label class="custom-control-label" for="rooftype_gable">
                                    {{ trans('page_calculator.roof-gable') }}
                                </label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="rooftype_canopy" name="roof_type"
                                       class="custom-control-input" value="canopy" m-model="form.roof_type">
                                <label class="custom-control-label" for="rooftype_canopy">
                                    {{ trans('page_calculator.roof-canopy') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="roof_width">{{ trans('page_calculator.total-roof-width') }}</label>
                            <input type="number"
                                   class="form-control"
                                   id="roof_width"
                                   name="roof_width"
                                   m-model="form.roof_width">
                        </div>
                        <div class="form-group">
                            <label for="roof_length">{{ trans('page_calculator.roof-length') }}</label>
                            <input type="number"
                                   class="form-control"
                                   id="roof_length"
                                   name="roof_length"
                                   m-model="form.roof_length">
                        </div>
                        <div class="form-group">
                            <label for="slope_angle">{{ trans('page_calculator.slope-angle') }}</label>
                            <input type="number"
                                   class="form-control"
                                   id="slope_angle"
                                   name="slope_angle"
                                   m-model="form.slope_angle">
                        </div>
                        <div class="form-row form-group">
                            <label class="col-12">
                                {{ trans('page_calculator.ratio') }}
                            </label>
                            <div class="w-100"></div>
                            <div class="col-5">
                                <label class="sr-only" for="rooftop_ratio_opaque">
                                    {{ trans('page_calculator.ratio-opaque') }}
                                </label>
                                <input type="number"
                                       class="form-control"
                                       id="rooftop_ratio_opaque"
                                       name="ratio_opaque"
                                       min="0"
                                       m-model="form.ratio_opaque">
                            </div>
                            <div class="col text-center">
                                :
                            </div>
                            <div class="col-5">
                                <label class="sr-only" for="rooftop_ratio_semiTR">
                                    {{ trans('page_calculator.ratio-semitr') }}
                                </label>
                                <input type="number"
                                       class="form-control"
                                       id="rooftop_ratio_semiTR"
                                       name="ratio_semiTR"
                                       min="0"
                                       m-model="form.ratio_semiTR">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit"
                                    m-literal:disabled="!readyToCalculate"
                                    class="btn btn-primary float-right"
                                    m-on:click.prevent="calculate">
                                {{ trans('page_calculator.calculate') }}
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="col-md-6 order-1 order-md-12">
                @include('calculator.gable-svg')
                @include('calculator.canopy-svg')
            </div>
        </section>
        <section class="row results" m-if="results" id="calculator_result" m-mask>
            <div class="col-md-4">
                <h4 class="results__title">{{ trans('page_calculator.results.profile') }}</h4>
                <dl>
                    <dt>{{ trans('page_calculator.results.required-rooftop-length') }}:</dt>
                    <dd>@{{ results.required_rooftop_length }}</dd>
                    <dt>{{ trans('page_calculator.results.required-amount-of-sheets') }}:</dt>
                    <dd>@{{ results.required_amount_of_sheets }}</dd>
                    <dt>{{ trans('page_calculator.results.amount-of-opaque') }}:</dt>
                    <dd>@{{ results.amount_of_opaque }}</dd>
                    <dt>{{ trans('page_calculator.results.amount-of-semiTR') }}:</dt>
                    <dd>@{{ results.amount_of_semiTR }}</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <h4 class="results__title">{{ trans('page_calculator.results.accessories') }}</h4>
                <dl>
                    <dt>{{ trans('page_calculator.results.required-amount-of-roofseal') }}:</dt>
                    <dd>@{{ results.required_amount_of_roofseal }}</dd>
                    <dt>{{ trans('page_calculator.results.required-amount-of-roofseal-bag') }}:</dt>
                    <dd>@{{ results.required_amount_of_roofseal_bag }}</dd>
                    <dt>{{ trans('page_calculator.results.required-amount-of-topridge') }}:</dt>
                    <dd>@{{ results.required_amount_of_topridge }}</dd>
                </dl>
            </div>
            <div class="col-md-4">
                <h4 class="results__title">{{ trans('page_calculator.results.cost') }}</h4>
                <dl>
                    <dt>{{ trans('page_calculator.results.total-cost-estimate') }}:</dt>
                    <dd>
                        <u>@{{ results.formatted_total_cost_estimate }}</u>
                    </dd>
                    <dt>{{ trans('page_calculator.results.total-roofseal-cost') }}:</dt>
                    <dd>@{{ results.formatted_total_roofseal_cost }}</dd>
                    <dt>{{ trans('page_calculator.results.total-topridge-cost') }}:</dt>
                    <dd>@{{ results.formatted_total_topridge_cost }}</dd>
                    <dt>{{ strtoupper(trans('page_calculator.results.total-cost-with-accessories')) }}:</dt>
                    <dd>
                        <strong>
                            <u>@{{ results.formatted_total_cost_with_accessories }}</u>
                        </strong>
                    </dd>
                </dl>
            </div>
            <div class="col-12 results__fineprint">
                <small>
                    * {{ trans('page_calculator.results.fineprint') }}
                </small>
            </div>
        </section>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => 'mailto:marketing@rooftop.co.id'])
        @slot('icon')
            <i class="cta__icon cta__icon--users"></i>
        @endslot
        {{ trans('cta.distributors-wanted') }}
        @slot('button')
            {{ trans('cta.send-us-email') }}
        @endslot
    @endcomponent
@endsection

@push('scripts')
    <script src="https://cdn.jsdelivr.net/combine/npm/moonjs@0.11.0,npm/axios@0.17.0/dist/axios.min.js"></script>
    <script>
      var calculatorApp = new Moon({
        el: '#calculator-page',
        data: {
          form: {
            roof_type: 'gable',
            roof_width: '',
            roof_length: '',
            slope_angle: 15,
            ratio_opaque: '',
            ratio_semiTR: ''
          },
          result: {},
          loading: false
        },
        methods: {
          calculate: function() {
            this.set('loading', true);
            axios.post('{{ route('api.calculator.calculate') }}', this.get('form')).then(function(response) {
              calculatorApp.set('results', response.data.data);
              calculatorApp.set('loading', false);
            });
          }
        },
        computed: {
          readyToCalculate: {
            get: function() {
              return this.get('form').roof_width && this.get('form').roof_length && this.get('form').ratio_opaque &&
                  this.get('form').ratio_semiTR;
            }
          }
        }
      });
    </script>
@endpush