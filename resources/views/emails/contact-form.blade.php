@component('vendor.mail.markdown.message')
# Contact Form Submission

@component('mail::table')
    | Field         | Content                           |
    | ------------- | --------------------------------- |
    | First Name    | **{{ $formData->first_name }}**   |
    | Last Name     | {{ $formData->last_name }}        |
    | Phone Number  | **{{ $formData->phone }}**        |
    | Company       | {{ $formData->company }}          |
    | Email         | **{{ $formData->email }}**        |
@endcomponent

Subject: **{{ $formData->subject }}**

`{{ nl2br($formData->message) }}`

@endcomponent
