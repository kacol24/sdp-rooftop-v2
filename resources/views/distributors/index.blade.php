@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-distributors.jpg', 'class' => 'bg-c-b'])
        {{ trans('page_distributor.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.authorized-distributors') }}
    @endcomponent
@endsection

@section('content')
    <div id="distributors-page" class="container">
        <div class="row">
            <div class="col">
                <div class="float-right mb-3">
                    <select id="city_changer" name="city" class="custom-select">
                        <option value="" selected>{{ trans('page_distributor.table.all-cities') }}</option>
                        @foreach($distributors->sortBy('location')->groupBy('location') as $city => $distributor)
                            <option value="{{ $city }}">{{ $city }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="table-responsive">
                    <table id="distributors_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>{{ trans('page_distributor.table.name') }}</th>
                            <th>{{ trans('page_distributor.table.details') }}</th>
                            <th data-dynatable-column="city">{{ trans('page_distributor.table.location') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($distributors as $distributor)
                            <tr>
                                <td>{!! $distributor->name !!}</td>
                                <td>
                                    {!! nl2br($distributor->details) !!}
                                </td>
                                <td>{{ strtoupper($distributor->location) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => 'mailto:marketing@rooftop.co.id'])
        @slot('icon')
            <i class="cta__icon cta__icon--users"></i>
        @endslot
        {{ trans('cta.distributors-wanted') }}
        @slot('button')
            {{ trans('cta.send-us-email') }}
        @endslot
    @endcomponent
@endsection

@push('scripts')
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/Dynatable/0.3.1/jquery.dynatable.min.js"></script>
    <script>
      $(function() {
        $('#distributors_table').dynatable({
          features: {
            paginate: false,
            recordCount: false,
            sorting: false,
            search: false
          },
          inputs: {
            queries: $('#city_changer')
          }
        });
      });
    </script>
@endpush