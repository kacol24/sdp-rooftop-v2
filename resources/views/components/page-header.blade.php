<header class="page-header {{ $class or '' }}" data-background="{{ cdn($image) }}">
    <div class="page-header__content container">
        <h1 class="page-header__title">
            {{ $slot }}
        </h1>
    </div>
</header>