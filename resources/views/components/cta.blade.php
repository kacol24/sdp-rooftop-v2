<div class="cta">
    <div class="container">
        <div class="row">
            @if(isset($icon))
                <div class="col-md-auto pr-xl-0 pr-lg-0 pr-md-0">
                    {{ $icon }}
                </div>
            @endif
            <div class="col-md-auto">
                <h3 class="cta__text h5">
                    {{ $slot }}
                </h3>
            </div>
            @if(isset($button))
                <div class="col-md-auto pl-xl-0 pl-lg-0 pl-md-0">
                    <a href="{{ $url or '#' }}" class="cta__button btn btn-primary">
                        {{ $button }}
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>