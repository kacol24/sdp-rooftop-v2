@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-blog.jpg'])
        {{ trans('page_blog.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.inform-progress') }}
    @endcomponent
@endsection

@section('content')
    <div id="blog-page" class="container">
        <div class="row">
            <div class="col-sm-12">
                @yield('main')
            </div>
            {{--<div class="col-sm-3">--}}
                {{--@component('blog._widget-archives')--}}
                {{--@endcomponent--}}
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => localized_route('contact.index')])
        @slot('icon')
            <i class="cta__icon cta__icon--question"></i>
        @endslot
        {{ trans('cta.unanswered-question') }}
        @slot('button')
            {{ trans('cta.contact-us') }}
        @endslot
    @endcomponent
@endsection