<!DOCTYPE html>
<html lang="{{ current_locale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! seo_helper()->render() !!}
    @stack('seo')

    <link rel="dns-prefetch" href="//code.jquery.com">
    <link rel="dns-prefetch" href="//cdn.jsdelivr.net">
    <link rel="dns-prefetch" href="//rooftop-cdn.jktglobalperkasa.co.id">
    <link rel="dns-prefetch" href="//ce8b0600c.cloudimg.io">
    <link rel="dns-prefetch" href="//use.fontawesome.com">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    {{--<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">--}}
    <script src="https://cdn.jsdelivr.net/combine/npm/lazysizes@4,npm/lazysizes@4/plugins/progressive/ls.progressive.min.js,npm/lazysizes@4/plugins/unveilhooks/ls.unveilhooks.min.js"
            async></script>
    {{--<script src="https://use.fontawesome.com/8223d1d78d.js" async defer></script>--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Raleway:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">--}}

<!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#3d92b9">

    {{--<link rel="stylesheet" href="{{ asset('css/vendors.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('css/app.css') }}">--}}
    <link rel="stylesheet" href="{{ cdn('css/app.css', 'https://rooftop-cdn.jktglobalperkasa.co.id') }}">

    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
        <link rel="alternate" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}"
              hreflang="{{ $localeCode }}">
    @endforeach

    @stack('styles')
</head>
<body class="@yield('body-class')">
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->

<!-- Add your site or application content here -->
<!-- Begin page content -->
<div id="wrapper">

    @if (session()->has('alert'))
        <div class="alert alert--fixed alert--fixed-top {{ session('alert')['type'] }} alert-dismissible {{ session('alert')['timeout'] ? 'alert-timeout' : '' }} text-center fade show"
             role="alert">
            {!! session('alert')['message'] !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            @if(session('alert')['timeout'])
                <div class="progress progress--fixed-bottom" style="height: 3px;">
                    <div class="progress-bar bg-success" role="progressbar" aria-valuenow="25" aria-valuemin="0"
                         aria-valuemax="100"></div>
                </div>
            @endif
        </div>
    @endif
    @php
        session()->flush();
    @endphp

    @include('includes.header')

    <div id="content" class="site-content">

        @yield('before-content')

        <main id="main" class="site-main" role="main">

            @yield('content')
        </main>

        @yield('after-content')

    </div>

    @include('includes.footer')
    <div class="language-switcher language-switcher--fixed">
        <a rel="alternate" hreflang="id"
           href="{{ LaravelLocalization::getLocalizedURL(current_locale() == 'en' ? 'id' : 'en', null, [], true) }}">
            @if (current_locale() == 'en')
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFASURBVHjaYrwvyMzw6S8DGPwD0//ACAj+wNj/kNgAAcTC8P6vUF87UPr/v38M//79//v3/18g+Yfh35//v//++/vn/x8g+v3/N4hxe9YigABiYWAGG/biOQNI6V+wNBj9/f0PqOj3738g1b////rFLCUNtAEggFgY/jIAjYSo/gdWygBU8ec3iP37z7/fv0DsXyARxj9AOQaAAGIBOe7b179fPv3/85cBah5Q6a9/v8HafoOM//frF1CckYf3FwMDQACxCOSmctjY//34EeSef2AEchiY8QfsB4jlf/8yCwiKnT8LEECMf/+CguY/EDCAIW7AxMT0/v17gABi+ffvHyMjI0g9Az7VEFmgLwACiAmoAb9SNG0AAQSyAWgXRA8DDADtZEABQC5IFqgYIIBAGn78+PEPAhjAEAeAaAUIMAD/YnbumkL3sQAAAABJRU5ErkJggg=="
                     alt="indonesian flag" title="Bahasa Indonesia" class="language-switcher__image">
                <span class="language-switcher__text">{{ trans('general.lang-id') }}</span>
            @else
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC"
                     alt="us flag" title="English" class="language-switcher__image">
                <span class="language-switcher__text">{{ trans('general.lang-en') }}</span>
            @endif
        </a>
    </div>
</div>

<a id="to_top" class="back-to-top back-to-top--fixed" href="#wrapper">
    <i class="fa fa-chevron-up"></i>
</a>

<script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write(
      '<script src="{{ cdn('js/jquery.slim.min.js', 'https://rooftop-cdn.jktglobalperkasa.co.id') }}"><\/script>');</script>
{{--<script src="{{ asset('js/vendors.js') }}"></script>--}}
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
<script src="{{ cdn('js/all.js', 'https://rooftop-cdn.jktglobalperkasa.co.id') }}"></script>
@stack('scripts')

{{--Analytics--}}
@if(config('app.env') == 'production')
    {{--Google Analytics--}}
    @php
        $googleAnalytics = new Arcanedev\SeoHelper\Entities\Analytics();
        $googleAnalytics->setGoogle('UA-63427645-2');
        echo $googleAnalytics->render();
    @endphp

    {{--Piwik--}}
    {{--@formatter:off--}}
    <script async defer>
        var _paq=_paq||[];_paq.push(["trackPageView"]),_paq.push(["enableLinkTracking"]),function(){var e="//piwik.sumberdjajaperkasa.com/";_paq.push(["setTrackerUrl",e+"piwik.php"]),_paq.push(["setSiteId",2]);var a=document,p=a.createElement("script"),r=a.getElementsByTagName("script")[0];p.type="text/javascript",p.async=!0,p.defer=!0,p.src=e+"piwik.js",r.parentNode.insertBefore(p,r)}();
    </script>
    <noscript><p><img src="//piwik.sumberdjajaperkasa.com/piwik.php?idsite=2" style="border:0;" alt=""/></p></noscript>
    {{--@formatter:on--}}
@endif
</body>
</html>