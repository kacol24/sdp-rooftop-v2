<div class="main-slide main-slide--full-height">
    <div class="overlay">
        <i class="fa fa-spinner fa-pulse fa-5x fa-fw"></i>
        <span class="sr-only">Loading...</span>
    </div>
    @foreach($slides as $slide)
        <div class="main-slide__item" data-background="{{ cdn($slide->image) }}">
            <img data-lazy="{{ cdn($slide->image) }}" style="display: none !important;">
            @if ($slide->cta)
                <div class="main-slide__content">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                @if ($slide->cta['text'])
                                    <div class="main-slide__text">
                                        {!! is_array($slide->cta['text']) ? $slide->cta['text'][LaravelLocalization::getCurrentLocale()] : $slide->cta['text'] !!}
                                    </div>
                                @endif
                                @if ($slide->cta['button'])
                                    <div class="main-slide__button">
                                        <a href="{{ LaravelLocalization::getLocalizedURL(null, $slide->cta['button']['url']) }}"
                                           class="btn btn-primary rounded-0">
                                            {{ is_array($slide->cta['button']['text']) ? $slide->cta['button']['text'][LaravelLocalization::getCurrentLocale()] : $slide->cta['button']['text'] }}
                                        </a>
                                    </div>
                                @endif
                                @if ($slide->extra)
                                    <div class="main-slide__extra mt-2">
                                        {!! $slide->extra !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    @endforeach
</div>

@push('scripts')
    <script>
      $(function(){var e=$(".main-slide"),a=$(".overlay");e.on("allImagesLoaded",function(){var i;a.addClass("hidden"),e.slick("slickGoTo",0),i=setTimeout(function(){a.remove(),clearTimeout(i)},1e3)}),e.slick({autoplay:!0,autoplaySpeed:5e3,dots:!0,fade:!0,lazyLoad:"progressive",slide:".main-slide__item"})});
    </script>
@endpush