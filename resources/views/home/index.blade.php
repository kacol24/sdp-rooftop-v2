@extends('layouts.master')

@section('body-class', 'homepage')

@section('before-content')
    @include('home._main-slide')
    @component('components.cta', ['url' => route('distributor.index')])
        @slot('icon')
            <i class="cta__icon cta__icon--map"></i>
        @endslot
        {{ trans('cta.where-to-buy') }}
        @slot('button')
            {{ trans('cta.locate-store-nearby') }}
        @endslot
    @endcomponent
@endsection

@section('content')
    <div id="home-page">
        <div class="background-overlay">
            @include('home._overview')
            @include('home._benefits')
        </div>
        <div class="clearfix"></div>
        @include('home._featured-projects')
        <div class="clearfix"></div>
        @include('home._more-links')
    </div>
@endsection