@php
    $featuredProjects = [
        (object)[
            'title' => 'Medical Device Manufacturer',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---1.jpg'
        ],
        (object)[
            'title' => 'Medical Device Manufacturer',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---2.jpg'
        ],
        (object)[
            'title' => 'Dian Regency II',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---3.jpg'
        ],
        (object)[
            'title' => 'Textile Manufacturer',
            'description' => 'Central Java, Indonesia',
            'image' => 'images/Project-Reference---4.jpg'
        ],
        (object)[
            'title' => 'Wisata Bahari Lamongan (WBL) Amusement Park',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---5.jpg'
        ],
        (object)[
            'title' => 'Surabaya Carnival Park Amusement Park',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---6.jpg'
        ],
        (object)[
            'title' => 'Jepara Ourland Park Amusement Park',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---7.jpg'
        ],
        (object)[
            'title' => 'The Orchard, Taman Dayu Commercial Area',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---8.jpg'
        ],
        (object)[
            'title' => 'Shoe Manufacturer',
            'description' => 'West Java, Indonesia',
            'image' => 'images/Project-Reference---9.jpg'
        ],
        (object)[
            'title' => 'Ciputra University',
            'description' => 'East Java, Indonesia',
            'image' => 'images/Project-Reference---10.jpg'
        ]
    ];
@endphp

<section id="featured_projects">
    <div class="container-fluid py-5 d-xl-none">
        <div class="row">
            <div class="col">
                <h2>{!! trans('page_home.reference.title') !!}</h2>
            </div>
        </div>
        <div class="row featured-projects__carousel">
            @foreach($featuredProjects as $featuredProject)
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="card">
                        <img class="card-img-top img-fluid w-100"
                             data-lazy="{{ cdn($featuredProject->image) }}"
                             alt="{{ $featuredProject->title }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $featuredProject->title }}</h5>
                            <p class="card-text">{{ $featuredProject->description }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row pt-5">
            <div class="col">
                {!! trans('page_home.reference.p') !!}
                <a href="{{ localized_route('project.index') }}" class="btn btn-primary">
                    {!! trans('page_home.reference.see-all-projects') !!}
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid px-0 d-none d-xl-block">
        <div class="row">
            <div class="col-xl-12" style="height: 50%">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="featured-projects__item lazyload"
                             data-bg="{{ cdn($featuredProjects[0]->image) }}">
                            <div class="featured-projects__overlay">
                                <div class="featured-projects__overlay-text">
                                    <h4 class="overlay-title">
                                        {{ $featuredProjects[0]->title }}
                                    </h4>
                                    <p class="overlay-description">
                                        {{ $featuredProjects[0]->description }}
                                    </p>
                                </div>
                            </div>
                            <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="featured-projects__item lazyload bg-c-c"
                                     data-bg="{{ cdn($featuredProjects[1]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[1]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[1]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[2]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[2]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[2]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[3]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[3]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[3]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 d-flex align-items-end">
                        <div class="featured-projects__description">
                            <h2>{!! trans('page_home.reference.title') !!}</h2>
                            {!! trans('page_home.reference.p') !!}
                            <a href="{{ localized_route('project.index') }}" class="btn btn-primary">
                                {!! trans('page_home.reference.see-all-projects') !!}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12" style="height: 50%">
                <div class="row">
                    <div class="col-xl-3">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[4]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[4]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[4]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[5]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[5]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[5]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[6]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[6]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[6]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[7]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[7]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[7]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="featured-projects__item lazyload"
                                     data-bg="{{ cdn($featuredProjects[8]->image) }}">
                                    <div class="featured-projects__overlay">
                                        <div class="featured-projects__overlay-text">
                                            <h4 class="overlay-title">
                                                {{ $featuredProjects[8]->title }}
                                            </h4>
                                            <p class="overlay-description">
                                                {{ $featuredProjects[8]->description }}
                                            </p>
                                        </div>
                                    </div>
                                    <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3">
                        <div class="featured-projects__item lazyload"
                             data-bg="{{ cdn($featuredProjects[9]->image) }}">
                            <div class="featured-projects__overlay">
                                <div class="featured-projects__overlay-text">
                                    <h4 class="overlay-title">
                                        {{ $featuredProjects[9]->title }}
                                    </h4>
                                    <p class="overlay-description">
                                        {{ $featuredProjects[9]->description }}
                                    </p>
                                </div>
                            </div>
                            <i class="fa fa-circle-o-notch fa-spin fa-fw fa-3x"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    <script>
      $(function(){$(".featured-projects__carousel").slick({mobileFirst:!0,dots:!0,lazyLoad:"progressive",slidesToShow:1,slidesToScroll:1,responsive:[{breakpoint:768,settings:{slidesToShow:2}},{breakpoint:992,settings:{slidesToShow:3}}]})});
    </script>
@endpush