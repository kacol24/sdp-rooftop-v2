<section id="benefits">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-xl-2 text-xl-right">
                <h2 class="mb-5 mb-xl-0">
                    {!! trans('page_home.benefits.title') !!}
                </h2>
            </div>
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item">
                            <i class="benefits__icon benefits__icon--outstanding"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.1') !!}</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item">
                            <i class="benefits__icon benefits__icon--never-corrode"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.2') !!}</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item">
                            <i class="benefits__icon benefits__icon--durable"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.3') !!}</div>
                        </div>
                    </div>
                    {{--<div class="w-100"></div>--}}
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item mb-5 mb-md-0">
                            <i class="benefits__icon benefits__icon--noise-insulation"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.4') !!}</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item mb-5 mb-md-0">
                            <i class="benefits__icon benefits__icon--chemical-resistance"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.5') !!}</div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="benefits__item mb-md-0">
                            <i class="benefits__icon benefits__icon--long-warranty"></i>
                            <div class="benefits__text">{!! trans('page_home.benefits.6') !!}</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-2 mt-sm-3">
                <a href="{{ localized_route('why-rooftop.index') }}" class="btn btn-outline-primary">
                    {!! trans('page_home.benefits.learn-more') !!}
                </a>
            </div>
        </div>
    </div>
</section>