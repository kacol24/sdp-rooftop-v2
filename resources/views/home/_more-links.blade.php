<section id="more-links">
    <div class="container-fluid" style="max-width: 1440px">
        <div class="row">
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card bg-dark text-white hover:slide-up">
                    <a href="{{ route('product.index') }}#installation">
                        <img class="card-img" src="{{ ci('images/box-link-installation.jpg', 'width/750/n') }}" alt="installation">
                        <div class="card-img-overlay">
                            <h4 class="card-title">{{ trans('page_home.more-links.installation') }}</h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card bg-dark text-white hover:slide-up">
                    <a href="{{ route('distributor.index') }}">
                        <img class="card-img" src="{{ ci('images/box-link-distributors.jpg', 'width/750/n') }}"
                             alt="nearest distributor">
                        <div class="card-img-overlay">
                            <h4 class="card-title">{{ trans('page_home.more-links.distributors') }}</h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card bg-dark text-white hover:slide-up">
                    <a href="{{ localized_route('contact.index') }}">
                        <img class="card-img" src="{{ ci('images/box-link-contact.jpg', 'width/750/n') }}" alt="contact us">
                        <div class="card-img-overlay">
                            <h4 class="card-title">{{ trans('page_home.more-links.contact') }}</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>