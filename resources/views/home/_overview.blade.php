<section id="overview">
    <div class="container">
        <div class="row">
            <div class="col-md-5 order-md-12 d-flex">
                <figure class="figure align-self-center" data-aos="fade-up">
                    <img src="{{ cdn('images/rooftop-tampak-depan.png') }}" alt="Rooftop Tampak Depan"
                         class="figure-img img-fluid">
                </figure>
            </div>
            <div class="col-md-7 order-md-1">
                <h2>
                    {!! trans('page_home.what-is-rooftop.title') !!}
                </h2>
                <p>{!! trans('page_home.what-is-rooftop.p') !!}</p>
                <a href="{{ localized_route('product.index') }}" class="btn btn-primary">
                    {!! trans('page_home.what-is-rooftop.button') !!}
                </a>
            </div>
        </div>
    </div>
</section>