@extends('layouts.master')

@section('before-content')
    @component('components.page-header', ['image' => 'images/headers/banner-contact.jpg'])
        {{ trans('page_contact.title') }}
    @endcomponent
    @component('components.cta')
        @slot('icon')
            <i class="cta__icon cta__icon--check"></i>
        @endslot
        {{ trans('cta.hear-from-you') }}
    @endcomponent
@endsection

@section('content')
    <div id="contact-page" class="container">
        <div class="row">
            <div class="col-sm-8">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="contact-form" method="POST" action="{{ route('contact.submit') }}">
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label for="first_name">{{ trans('page_contact.form.first-name') }}*</label>
                            <input type="text" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                   id="first_name" name="first_name" value="{{ old('first_name') }}" required>
                            @if($errors->has('first_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('first_name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="last_name">{{ trans('page_contact.form.last-name') }}</label>
                            <input type="text" class="form-control" id="last_name" name="last_name"
                                   value="{{ old('last_name') }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-6">
                            <label for="phone">{{ trans('page_contact.form.phone') }}*</label>
                            <input type="text" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                   id="phone" name="phone" value="{{ old('phone') }}" required>
                            @if($errors->has('phone'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('phone') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="company">{{ trans('page_contact.form.company') }}</label>
                            <input type="text" class="form-control" id="company" name="company"
                                   value="{{ old('company') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email">{{ trans('page_contact.form.email') }}*</label>
                        <input type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                               id="email" name="email" value="{{ old('email') }}" required>
                        @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="subject">{{ trans('page_contact.form.subject') }}</label>
                        <input type="text" class="form-control" id="subject" name="subject"
                               value="{{ old('subject') }}">
                    </div>
                    <div class="form-group">
                        <label for="message">{{ trans('page_contact.form.message') }}</label>
                        <textarea class="form-control" id="message" rows="3"
                                  name="message">{{ old('message') }}</textarea>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox"
                                   class="custom-control-input {{ $errors->has('agreement') ? 'is-invalid' : '' }}"
                                   name="agreement" id="agreement" required>
                            <label class="custom-control-label" for="agreement">
                                @lang('page_contact.form.agree')
                            </label>
                            @if($errors->has('agreement'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('agreement') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    @captcha(app()->getLocale())
                    <button type="submit" class="btn btn-primary mt-3">{{ trans('page_contact.form.send') }}</button>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="embed-responsive embed-responsive-1by1">
                    <iframe class="embed-responsive-item lazyload"
                            data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7912.307163700837!2d112.64360853147956!3d-7.448254975790231!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7e19400cdd34b%3A0x5be629b3b8516396!2sPT.+Sumber+Djaja+Perkasa!5e0!3m2!1sen!2sid!4v1435833398115"
                            width="100%" frameborder="0" style="border:0"></iframe>
                </div>
                <address>
                    <strong>{{ trans('page_contact.factory') }}:</strong><br>
                    Jl. Raya Pilang KM 8 No. 88<br>
                    Kec. Wonoayu Sidoarjo 61273<br>
                    Telp: <a href="tel:+62318855588">+62 31 885 5588</a><br>
                    Fax: <a href="tel:+62318855799">+62 31 885 5799</a>
                </address>
                <div class="embed-responsive embed-responsive-1by1">
                    <iframe class="embed-responsive-item lazyload"
                            data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.029838421668!2d112.74185999999999!3d-7.237436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7f9179d261e05%3A0x7bca77b7c7a8a8e7!2sJl.+Kembang+Jepun+No.94%2C+Pabean+Cantian%2C+Kota+SBY%2C+Jawa+Timur+60162!5e0!3m2!1sen!2sid!4v1435833293371"
                            width="100%" frameborder="0" style="border:0"></iframe>
                </div>
                <address>
                    <strong>{{ trans('page_contact.office-sby') }}:</strong><br>
                    Jl. Kembang Jepun 94<br>
                    Surabaya 60162<br>
                    Telp: <a href="tel:+62313539900">+62 31 353 9900</a><br>
                    Fax: <a href="tel:+62313531189">+62 31 353 1189</a>
                </address>
                <div class="embed-responsive embed-responsive-1by1">
                    <iframe class="embed-responsive-item lazyload"
                            data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.926917000506!2d106.73203919999999!3d-6.1405199!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f7fda898f63b%3A0xd51c61544c9f6150!2sJl.+Taman+Palem+Mutiara+Blok+B3+No.17%2C+Cengkareng%2C+Kota+Jkt+Bar.%2C+Daerah+Khusus+Ibukota+Jkt+11730!5e0!3m2!1sen!2sid!4v1435917893778"
                            width="100%" frameborder="0" style="border:0"></iframe>
                </div>
                <address>
                    <strong>{{ trans('page_contact.office-jkt') }}:</strong><br>
                    Ruko Mutiara Taman Palem B3-17<br>
                    Cengkareng, Jakarta Barat<br>
                    Telp: <a href="tel:+622129024576">+62 21 2902 4576</a>
                </address>
            </div>
        </div>
    </div>
@endsection

@section('after-content')
    @component('components.cta', ['url' => 'mailto:marketing@rooftop.co.id'])
        @slot('icon')
            <i class="cta__icon cta__icon--users"></i>
        @endslot
        {{ trans('cta.distributors-wanted') }}
        @slot('button')
            {{ trans('cta.send-us-email') }}
        @endslot
    @endcomponent
@endsection