@php
    seo_helper()->setTitle(trans('errors.404.title'))
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="col-12 text-center text-primary">
                @lang('errors.404.title')
            </h1>
            <p class="col-12 text-center">
                @lang('errors.404.text')
            </p>
        </div>
    </div>
@endsection