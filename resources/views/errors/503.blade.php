@php
    seo_helper()->setTitle(trans('errors.503.title'))
@endphp

@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="col-12 text-center text-primary">
                @lang('errors.503.title')
            </h1>
            <p class="col-12 text-center">
                @lang('errors.503.text')
            </p>
        </div>
    </div>
@endsection