@extends('layouts.blog-sidebar')

@section('main')
    @forelse($posts->chunk(2) as $row)
        <div class="card-deck">
            @foreach($row as $post)
                <a class="card mb-3 col-md-6 px-0"
                   href="{{ LaravelLocalization::getNonLocalizedURL(route('blog.show', $post->slug)) }}">
                    @if (isset($post->featured_image))
                        <img class="card-img-top img-fluid"
                             src="https://ce8b0600c.cloudimg.io/fit/600x450/n/{{ asset('uploads/blog/' . $post->featured_image) }}"
                             src="{{ Croppa::url('images/manipulated/uploads/blog/'.$post->featured_image, 600, 450, ['resize', 'pad']) }}"
                             alt="{{ $post->title }}">
                    @endif
                    <div class="card-body">
                        <h4 class="card-title">{!! $post->title !!}</h4>
                        <div class="card-text">
                            @if ($post->excerpt != '')
                                {!! $post->excerpt !!}
                            @else
                                {!! str_limit($post->body, 100) !!}
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">
                            Posted {{ $post->created_at->diffForHumans() }}
                            <em>({{ $post->created_at->toFormattedDateString('d-m-Y') }})</em>
                        </small>
                    </div>
                </a>
            @endforeach
        </div>
    @empty
        <h3>Oops! There are no posts yet.</h3>
        <p>Please stay tuned, more posts are coming in the near future.</p>
    @endforelse
    @if($posts->count())
        <div class="row">
            <div class="col-12">
                {!! $posts->links('blog._pagination', ['posts' => $posts]) !!}
            </div>
        </div>
    @endif
@endsection