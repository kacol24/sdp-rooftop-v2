@extends('layouts.blog-sidebar')

@section('language-switcher')
    @if(isset($post->alternate_slug))
        <li class="list-inline-item mr-0">
            <a rel="alternate" hreflang="id" class="nav-link"
               href="{{ LaravelLocalization::getLocalizedURL('id', route('blog.show', $post->locale == 'id' ? $post->slug : $post->alternate_slug), [], true) }}">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFASURBVHjaYrwvyMzw6S8DGPwD0//ACAj+wNj/kNgAAcTC8P6vUF87UPr/v38M//79//v3/18g+Yfh35//v//++/vn/x8g+v3/N4hxe9YigABiYWAGG/biOQNI6V+wNBj9/f0PqOj3738g1b////rFLCUNtAEggFgY/jIAjYSo/gdWygBU8ec3iP37z7/fv0DsXyARxj9AOQaAAGIBOe7b179fPv3/85cBah5Q6a9/v8HafoOM//frF1CckYf3FwMDQACxCOSmctjY//34EeSef2AEchiY8QfsB4jlf/8yCwiKnT8LEECMf/+CguY/EDCAIW7AxMT0/v17gABi+ffvHyMjI0g9Az7VEFmgLwACiAmoAb9SNG0AAQSyAWgXRA8DDADtZEABQC5IFqgYIIBAGn78+PEPAhjAEAeAaAUIMAD/YnbumkL3sQAAAABJRU5ErkJggg=="
                     alt="indonesian flag" title="{{ trans('general.lang-id') }}">
            </a>
        </li>
        <li class="list-inline-item mx-0">
            //
        </li>
        <li class="list-inline-item ml-0">
            <a rel="alternate" hreflang="en" class="nav-link"
               href="{{ LaravelLocalization::getLocalizedURL('en', route('blog.show', $post->locale == 'en' ? $post->slug : $post->alternate_slug), [], true) }}">
                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC"
                     alt="us flag" title="{{ trans('general.lang-en') }}">
            </a>
        </li>
    @endif
@endsection

@section('main')
    <article class="blog">
        <header class="blog__header">
            @if(isset($post->featured_image))
                <figure class="figure">
                    <img src="{{ asset('uploads/blog/'. $post->featured_image) }}" alt="{{ $post->title }}"
                         class="figure-img img-fluid blog__featured-image">
                </figure>
            @endif
            <h2 class="blog__title">{{ $post->title }}</h2>
            <div class="blog__meta">
                <div class="blog__meta-item">
                    <i class="fa fa-calendar"></i> <span
                            title="{{ $post->created_at }}">{{ $post->created_at->toDateString() }}</span>
                </div>
            </div>
        </header>
        @if($post->excerpt != '')
            <div class="blog__excerpt" style="line-height: 2;">
                {!! $post->excerpt !!}
            </div>
            <hr>
        @endif
        <div class="blog__content" style="line-height: 2;">
            {!! nl2br($post->body) !!}

            @if(isset($post->images))
                <ul class="list-unstyled" id="blog_carousel">
                    @if(is_array($post->images))
                        @foreach($post->images as $image)
                            <li>
                                <a href="{{ asset('uploads/blog/'. $image) }}" data-fancybox="{{ $post->slug }}">
                                    @if($loop->first)
                                        <img src="{{ asset('uploads/blog/'. $image) }}" class="img-fluid img-thumbnail">
                                    @else
                                        <img data-lazy="{{ asset('uploads/blog/'. $image) }}"
                                             class="img-fluid img-thumbnail">
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    @else
                        @php
                            $images = Storage::disk('uploads')->files('uploads/blog/' . $post->images);
                        @endphp
                        @foreach($images as $image)
                            <li>
                                <a href="{{ asset($image) }}" data-fancybox="{{ $post->slug }}">
                                    @if($loop->first)
                                        <img src="{{ asset($image) }}" class="img-fluid img-thumbnail">
                                    @else
                                        <img data-lazy="{{ asset($image) }}"
                                             class="img-fluid img-thumbnail">
                                    @endif
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
                @push('styles')
                    <link rel="stylesheet"
                          href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css"/>
                @endpush
                @push('scripts')
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
                    <script>
                      $(function() {
                        var $blogCarousel = $('#blog_carousel');
                        $blogCarousel.slick({
                          slidesToShow: 3,
                          dots: true,
                          infinite: false,
                          arrows: false,
                          responsive: [
                            {
                              breakpoint: 768,
                              settings: {
                                slidesToShow: 1,
                                adaptiveHeight: true
                              }
                            }
                          ]
                        });
                        $('[data-fancybox]').fancybox({
                          thumbs: {
                            autoStart: true
                          }
                        });
                      });
                    </script>
                @endpush
            @endif
        </div>
        <footer class="blog__footer">
            share buttons
        </footer>
    </article>
@endsection