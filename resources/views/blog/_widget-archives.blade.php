<div class="widget">
    <h3 class="widget__title">{{ trans('page_blog.archives') }}</h3>
    <div class="widget__content">
        <ol class="list-unstyled">
            @foreach($postArchives as $year => $archive)
                <li>
                    <a href="">{{ $year }} ({{ $archive->count() }})</a>
                </li>
            @endforeach
        </ol>
    </div>
</div>