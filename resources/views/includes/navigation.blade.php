<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="{{ localized_route('index') }}">
            <img src="{{ cdn('images/logo.png') }}" alt="Rooftop Logo" title="Rooftop®"
                 class="site-logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#main-navigation"
                aria-controls="main-navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="main-navigation">
            <ul class="navbar-nav text-md-center w-100">
                <li class="nav-item {{ active_class(if_route('product.index')) }}">
                    <a class="nav-link" href="{{ localized_route('product.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.product') }}
                            {!! active_class(if_route(('product.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route_pattern('why-rooftop.*')) }}">
                    <a class="nav-link" href="{{ localized_route('why-rooftop.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.why-rooftop') }}
                            {!! active_class(if_route(('why-rooftop.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('project.index')) }}">
                    <a class="nav-link" href="{{ localized_route('project.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.project') }}
                            {!! active_class(if_route(('project.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('pricelist.index')) }}">
                    <a class="nav-link" href="{{ localized_route('pricelist.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.pricelist') }}
                            {!! active_class(if_route(('pricelist.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('contact.index')) }}">
                    <a class="nav-link" href="{{ localized_route('contact.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.contact') }}
                            {!! active_class(if_route(('contact.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav text-md-center w-100 navbar-secondary">
                <li class="nav-item {{ active_class(if_route('our-company.index')) }}">
                    <a class="nav-link" href="{{ localized_route('our-company.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.our-company') }}
                            {!! active_class(if_route(('our-company.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('blog.index')) }}">
                    <a class="nav-link" href="{{ localized_route('blog.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.blog') }}
                            {!! active_class(if_route(('blog.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('distributor.index')) }}">
                    <a class="nav-link" href="{{ localized_route('distributor.index') }}">
                        <div class="link-wrapper">
                            {{ trans('nav.distributor') }}
                            {!! active_class(if_route(('distributor.index')), '<span class="sr-only">(current)</span>') !!}
                        </div>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('faq.index')) }}">
                    <a class="nav-link" href="{{ localized_route('faq.index') }}">
                        <span class="link-wrapper">
                            {{ trans('nav.faq') }}
                            {!! active_class(if_route(('faq.index')), '<span class="sr-only">(current)</span>') !!}
                        </span>
                    </a>
                </li>
                <li class="nav-item {{ active_class(if_route('calculator.index')) }}">
                    <a class="nav-link" href="{{ localized_route('calculator.index') }}">
                        <span class="link-wrapper">
                            {{ trans('nav.calculator') }}
                            {!! active_class(if_route(('calculator.index')), '<span class="sr-only">(current)</span>') !!}
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>