<header class="site-header sticky-top">
    <div class="top-bar">
        @include('includes.top-bar')
    </div>
    <div class="main-menu">
        @include('includes.navigation')
    </div>
</header>