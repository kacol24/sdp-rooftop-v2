<section class="sitemap sitemap--inverted" data-background="{{ cdn('images/sitemap-overlay.png') }}">
    @include('includes.distributors-carousel')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3">
                <div class="sitemap__menu sitemap__menu--first">
                    <h5 class="sitemap__menu-title">{{ trans('sitemap.contact-us') }}</h5>
                    <address>
                        <strong>{{ strtoupper(trans('sitemap.factory')) }}</strong><br>
                        Jl. Raya Pilang KM 8 No. 88<br>
                        Kec. Wonoayu Sidoarjo 61273<br>
                        Telp: <a href="tel:+62318855588">+62 31 885 5588</a><br>
                        Fax: +62 31 885 5799
                    </address>
                </div>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="row">
                    <div class="col-md">
                        <div class="sitemap__menu">
                            <h5 class="sitemap__menu-title">{{ trans('sitemap.our-company') }}</h5>
                            <ul class="sitemap__menu-list">
                                <li>
                                    <a href="{{ localized_route('our-company.index') }}">
                                        {{ trans('nav.our-company') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('contact.index') }}">
                                        {{ trans('nav.contact') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('blog.index') }}">
                                        {{ trans('nav.blog') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('distributor.index') }}">
                                        {{ trans('nav.distributor') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('pricelist.index') }}">
                                        {{ trans('nav.pricelist') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="sitemap__menu">
                            <h5 class="sitemap__menu-title">{{ trans('sitemap.product') }}</h5>
                            <ul class="sitemap__menu-list">
                                <li>
                                    <a href="{{ localized_route('product.index') }}">
                                        Rooftop®
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('why-rooftop.index') }}">
                                        {{ trans('sitemap.rooftop-benefits') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('why-rooftop.index') }}">
                                        {{ trans('sitemap.rooftop-vs-competitor') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('project.index') }}">
                                        {{ trans('sitemap.projects-portfolio') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('calculator.index') }}">
                                        {{ trans('sitemap.estimate-cost') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ localized_route('faq.index') }}">
                                        {{ trans('nav.faq') }}
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="sitemap__menu">
                            <h5 class="sitemap__menu-title">{{ trans('sitemap.other-links') }}</h5>
                            <ul class="sitemap__menu-list">
                                <li>
                                    <a target="_blank" href="https://sumberdjajaperkasa.com/">
                                        PT. Sumber Djaja Perkasa
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.formaxroof.co.id/">
                                        Formax Roof
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.royalroof.co.id/">
                                        Royal Roof
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.duma.co.id/">
                                        Duma Premium WPC
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.luminate.co.id/">
                                        Luminate Aluminum
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://www.regisfloor.co.id/">
                                        Regis Premium Vinyl Flooring
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="sitemap__menu">
                            <h5 class="sitemap__menu-title">{{ trans('sitemap.connect') }}</h5>
                            <ul class="sitemap__menu-list">
                                <li>
                                    <a target="_blank" href="https://www.facebook.com/atapdingin.SDP">
                                        Facebook
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="https://www.instagram.com/rooftop.id/">
                                        Instagram
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank"
                                       href="https://www.youtube.com/channel/UCYMUBj7A3suek6-lSAxfpIA/?ab_channel=AtapDingin-Rooftop%2CFormax%2CRoyalRoof">
                                        Youtube
                                    </a>
                                </li>
                                <li>
                                    <a target="_blank" href="mailto:marketing@sumberdjajaperkasa.com">
                                        E-mail
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>