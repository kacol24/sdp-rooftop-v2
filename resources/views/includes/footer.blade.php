@include('includes.sitemap')
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 order-sm-12 site-footer__social-icons">
                <ul class="list-inline">
                    @foreach(config('sdp.social_medias') as $socialMedia)
                        <li class="list-inline-item">
                            <a href="{{ $socialMedia['url'] }}" class="nav-link" target="_blank">
                                <i class="fa {{ $socialMedia['icon'] }}"></i>
                                <span class="sr-only">{{ $socialMedia['title'] }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-6 order-sm-1 site-footer__copyright">
                {{ trans('general.copyright') }} &copy; {{ date('Y') }} PT. Sumber Djaja
                Perkasa. {{ trans('general.rights-reserveed') }}
            </div>
        </div>
    </div>
</footer>