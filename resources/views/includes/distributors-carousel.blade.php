@inject('distributorLogos', 'App\Repositories\Contracts\DistributorLogoRepository')

<section class="distributors">
    <div class="container">
        <div class="row">
            <div class="col">
                <h6 class="text-center">{{ trans('sitemap.authorized-distributors') }}:</h6>
            </div>
        </div>
        <div class="distributors__carousel row">
            @foreach($distributorLogos->all() as $logo)
                <div class="col-lg-2 distributors__carousel-item">
                    <img src="{{ ci($logo->url, 'width/400/n') }}" class="img-fluid mx-auto" alt="{{ $logo->name }}"
                         title="{{ $logo->name }}">
                </div>
            @endforeach
        </div>
    </div>
</section>

@push('scripts')
    <script>
      $(function(){$(".distributors__carousel").slick({autoplay:!0,arrows:!1,centerMode:!0,dots:!0,mobileFirst:!0,slide:".distributors__carousel-item",responsive:[{breakpoint:768,settings:{slidesToShow:3}},{breakpoint:992,settings:"unslick"}]})});
    </script>
@endpush