<nav class="navbar navbar-expand-lg">
    <div class="container">
        <ul class="navbar-nav text-md-center nav-justified w-100">
            <li class="nav-item">
                <ul class="list-inline social-icons">
                    @foreach(config('sdp.social_medias') as $socialMedia)
                        <li class="list-inline-item">
                            <a href="{{ $socialMedia['url'] }}" class="nav-link" target="_blank">
                                <i class="fa {{ $socialMedia['icon'] }}"></i>
                                <span class="sr-only">{{ $socialMedia['title'] }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li class="nav-item {{ active_class(if_route('our-company.index')) }}">
                <a class="nav-link" href="{{ localized_route('our-company.index') }}">
                    {{ trans('nav.our-company') }}
                    {!! active_class(if_route(('our-company.index')), '<span class="sr-only">(current)</span>') !!}
                </a>
            </li>
            <li class="nav-item {{ active_class(if_route_pattern('blog.*')) }}">
                <a class="nav-link" href="{{ localized_route('blog.index') }}">
                    {{ trans('nav.blog') }}
                    {!! active_class(if_route(('blog.index')), '<span class="sr-only">(current)</span>') !!}
                </a>
            </li>
            <li class="nav-item {{ active_class(if_route('distributor.index')) }}">
                <a class="nav-link" href="{{ localized_route('distributor.index') }}">
                    {{ trans('nav.distributor') }}
                    {!! active_class(if_route(('distributor.index')), '<span class="sr-only">(current)</span>') !!}
                </a>
            </li>
            <li class="nav-item {{ active_class(if_route('faq.index')) }}">
                <a class="nav-link" href="{{ localized_route('faq.index') }}">
                    {{ trans('nav.faq') }}
                    {!! active_class(if_route(('faq.index')), '<span class="sr-only">(current)</span>') !!}
                </a>
            </li>
            <li class="nav-item {{ active_class(if_route('calculator.index')) }}">
                <a class="nav-link" href="{{ localized_route('calculator.index') }}">
                    {{ trans('nav.calculator') }}
                    {!! active_class(if_route(('calculator.index')), '<span class="sr-only">(current)</span>') !!}
                </a>
            </li>
            <li class="nav-item">
                <ul class="list-inline">
                    @section('language-switcher')
                        <li class="list-inline-item mr-0">
                            <a rel="alternate" hreflang="id" class="nav-link"
                               href="{{ LaravelLocalization::getLocalizedURL('id', null, [], true) }}">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAFASURBVHjaYrwvyMzw6S8DGPwD0//ACAj+wNj/kNgAAcTC8P6vUF87UPr/v38M//79//v3/18g+Yfh35//v//++/vn/x8g+v3/N4hxe9YigABiYWAGG/biOQNI6V+wNBj9/f0PqOj3738g1b////rFLCUNtAEggFgY/jIAjYSo/gdWygBU8ec3iP37z7/fv0DsXyARxj9AOQaAAGIBOe7b179fPv3/85cBah5Q6a9/v8HafoOM//frF1CckYf3FwMDQACxCOSmctjY//34EeSef2AEchiY8QfsB4jlf/8yCwiKnT8LEECMf/+CguY/EDCAIW7AxMT0/v17gABi+ffvHyMjI0g9Az7VEFmgLwACiAmoAb9SNG0AAQSyAWgXRA8DDADtZEABQC5IFqgYIIBAGn78+PEPAhjAEAeAaAUIMAD/YnbumkL3sQAAAABJRU5ErkJggg=="
                                     alt="indonesian flag" title="{{ trans('general.lang-id') }}">
                            </a>
                        </li>
                        <li class="list-inline-item mx-0">
                            //
                        </li>
                        <li class="list-inline-item ml-0">
                            <a rel="alternate" hreflang="en" class="nav-link"
                               href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAIAAAD5gJpuAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHzSURBVHjaYkxOP8IAB//+Mfz7w8Dwi4HhP5CcJb/n/7evb16/APL/gRFQDiAAw3JuAgAIBEDQ/iswEERjGzBQLEru97ll0g0+3HvqMn1SpqlqGsZMsZsIe0SICA5gt5a/AGIEarCPtFh+6N/ffwxA9OvP/7//QYwff/6fZahmePeB4dNHhi+fGb59Y4zyvHHmCEAAAW3YDzQYaJJ93a+vX79aVf58//69fvEPlpIfnz59+vDhw7t37968efP3b/SXL59OnjwIEEAsDP+YgY53b2b89++/awvLn98MDi2cVxl+/vl6mituCtBghi9f/v/48e/XL86krj9XzwEEEENy8g6gu22rfn78+NGs5Ofr16+ZC58+fvyYwX8rxOxXr169fPny+fPn1//93bJlBUAAsQADZMEBxj9/GBxb2P/9+S/R8u3vzxuyaX8ZHv3j8/YGms3w8ycQARmi2eE37t4ACCDGR4/uSkrKAS35B3TT////wADOgLOBIaXIyjBlwxKAAGKRXjCB0SOEaeu+/y9fMnz4AHQxCP348R/o+l+//sMZQBNLEvif3AcIIMZbty7Ly6t9ZmXl+fXj/38GoHH/UcGfP79//BBiYHjy9+8/oUkNAAHEwt1V/vI/KBY/QSISFqM/GBg+MzB8A6PfYC5EFiDAABqgW776MP0rAAAAAElFTkSuQmCC"
                                     alt="us flag" title="{{ trans('general.lang-en') }}">
                            </a>
                        </li>
                    @show
                </ul>
            </li>
        </ul>
    </div>
</nav>