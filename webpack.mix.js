let mix = require('laravel-mix');
let glob = require('glob-all');
let purgeCss = require('purgecss-webpack-plugin');

// mix.copyDirectory('node_modules/slick-carousel/slick/fonts', 'public/fonts');
// mix.copy('node_modules/slick-carousel/slick/ajax-loader.gif', 'public/images');

mix.combine([
  // 'node_modules/lazysizes/lazysizes.min.js',
  // 'node_modules/lazysizes/plugins/progressive/ls.progressive.min.js',
  // 'node_modules/lazysizes/plugins/unveilhooks/ls.unveilhooks.min.js',
  // 'node_modules/popper.js/dist/umd/popper.min.js',
  'resources/assets/js/font-awesome-4.7.0.js',
  'node_modules/bootstrap/js/dist/util.js',
  'node_modules/bootstrap/js/dist/alert.js',
  'node_modules/bootstrap/js/dist/modal.js',
  'node_modules/bootstrap/js/dist/collapse.js',
  'node_modules/slick-carousel/slick/slick.min.js',
  'node_modules/aos/dist/aos.js'
], 'public/js/vendors.js');

mix.js('resources/assets/js/app.js', 'public/js');
mix.combine([
  'public/js/vendors.js',
  'public/js/app.js'
], 'public/js/all.js');
mix.sass('resources/assets/sass/app.scss', 'public/css');

if (mix.inProduction()) {
  mix.webpackConfig({
    plugins: [
      new purgeCss({
        paths: glob.sync([
          path.join(__dirname, 'app/**/*.php'),
          path.join(__dirname, 'resources/views/**/*.blade.php')
        ]),
        whitelistPatterns: [/slick/, /lazy/, /show/, /collap/, /aos/, /modal/, /card/, /card-deck/, /card-body/, /card-footer/],
        extractors: [
          {
            extractor: class {
              static extract(content) {
                return content.match(/[A-z0-9-:\/]+/g);
              }
            },
            extensions: ['html', 'js', 'php', 'vue']
          }
        ]
      })
    ]
  });
}

mix.browserSync({
  host: 'rooftop-v2.test',
  proxy: 'rooftop-v2.test',
  open: false
});