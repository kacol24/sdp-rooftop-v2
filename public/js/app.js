/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// import 'bootstrap';
// import 'bootstrap/js/dist/util';
// import 'bootstrap/js/dist/button';
// import 'bootstrap/js/dist/modal';
// import 'bootstrap/js/dist/collapse';

var Rooftop = function ($) {
  function _dismissAlert() {
    var timeout = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 5000;

    var $alert = $('.alert-timeout');
    $alert.each(function () {
      $alert.find('.progress .progress-bar').css('width', 0);
      $alert.delay(timeout).slideUp();
    });
  }

  function _replaceBackground() {
    var $elements = $('[data-background]');
    $elements.each(function () {
      var $this = $(this);
      var url = $this.data('background');
      $this.css('background-image', 'url(' + url + ')');
    });
  }

  function _backToTop() {
    var $element = $('#to_top');
    $(window).scroll(function () {
      if ($(this).scrollTop() > 500) {
        $element.fadeIn(300);
      } else {
        $element.fadeOut(300);
      }
    });
    $element.click(function (o) {
      o.preventDefault();
      $('html, body').animate({ scrollTop: 0 }, 500);
    });
  }

  function _initAOS() {
    AOS.init({
      duration: 1000,
      once: true
    });
  }

  function adjustMainSlideHeight() {
    var $window = $(window);
    var $header = $('header.site-header');
    var $slide = $('.main-slide--full-height');

    var windowHeight = $window.height();
    var headerHeight = $header.height();
    var slideHeight = windowHeight - headerHeight;
    $slide.height(slideHeight);
  }

  function init() {
    _replaceBackground();
    adjustMainSlideHeight();
    _backToTop();
    _initAOS();
    _dismissAlert();
  }

  return {
    init: init,
    adjustMainSlideHeight: adjustMainSlideHeight
  };
}(jQuery);

$(function () {
  Rooftop.init();
});

var resizeTimer = void 0;
$(window).on('resize', function () {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function () {
    Rooftop.adjustMainSlideHeight();
  }, 250);
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);