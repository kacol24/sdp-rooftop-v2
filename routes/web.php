<?php
Route::get('flush-cache', 'CacheController@flush');

Route::group(
    [
        'prefix'     => LaravelLocalization::setLocale(),
        'middleware' => ['localize', 'localeSessionRedirect', 'localizationRedirect'],
    ],
    function () {
        Route::get('/', 'HomeController@index')
             ->name('home.index');
        Route::get(LaravelLocalization::transRoute('routes.product.index'), 'ProductController@index')
             ->name('product.index');
        Route::get(LaravelLocalization::transRoute('routes.why-rooftop.index'), 'WhyController@index')
             ->name('why-rooftop.index');
        Route::get(LaravelLocalization::transRoute('routes.why-rooftop.compare'), 'WhyController@compare')
             ->name('why-rooftop.compare');
        Route::get(LaravelLocalization::transRoute('routes.project.index'), 'ProjectController@index')
             ->name('project.index');
        Route::get(LaravelLocalization::transRoute('routes.project.show'), 'ProjectController@show')
             ->name('project.show');
        Route::get(LaravelLocalization::transRoute('routes.pricelist.index'), 'PricelistController@index')
             ->name('pricelist.index');
        Route::get(LaravelLocalization::transRoute('routes.contact.index'), 'ContactController@index')
             ->name('contact.index');
        Route::post(LaravelLocalization::transRoute('routes.contact.index'), 'ContactController@submit')
             ->name('contact.submit');
        
        Route::get(LaravelLocalization::transRoute('routes.our-company.index'), 'OurCompanyController@index')
             ->name('our-company.index');
        Route::get(LaravelLocalization::transRoute('routes.blog.index'), 'BlogController@index')
             ->name('blog.index');
        Route::get(LaravelLocalization::transRoute('routes.distributor.index'), 'DistributorController@index')
             ->name('distributor.index');
        Route::get(LaravelLocalization::transRoute('routes.faq.index'), 'FaqController@index')
             ->name('faq.index');
        Route::get(LaravelLocalization::transRoute('routes.calculator.index'), 'CalculatorController@index')
             ->name('calculator.index');
        
        Route::get('/{slug}', 'BlogController@show')->name('blog.show');
    }
);

Route::post('api/calculate', 'CalculatorController@calculate')
     ->name('api.calculator.calculate');