<?php

return [
    /* -----------------------------------------------------------------
     |  Title
     | -----------------------------------------------------------------
     */
    'title'       => [
        'default'   => 'Know More About Rooftop®',
        'site-name' => 'Atap Dingin Rooftop®',
        'separator' => ' | ',
        'first'     => true,
        'max'       => 250,
    ],
    
    /* -----------------------------------------------------------------
     |  Description
     | -----------------------------------------------------------------
     */
    'description' => [
        'default' => 'The best solution for your roofing investment, We are proud to offer Rooftop high performance PVC roofing solutions representing only the finest manufacturers of equipment, with the technical know-how and support required, to provide total roofing peace of mind from project design to completion andbeyond.',
        'max'     => 500,
    ],
    
    /* -----------------------------------------------------------------
     |  Keywords
     | -----------------------------------------------------------------
     */
    'keywords'    => [
        'default' => ['atap dingin', 'rooftop', 'atap pvc', 'sumber djaja perkasa', 'roof top'],
    ],
    
    /* -----------------------------------------------------------------
     |  Miscellaneous
     | -----------------------------------------------------------------
     */
    'misc'        => [
        'canonical' => true,
        'robots'    => config('app.env') !== 'production',
        // Tell robots not to index the content if it's not on production
        'default'   => [
            'author'    => '', // https://plus.google.com/[YOUR PERSONAL G+ PROFILE HERE]
            'publisher' => '', // https://plus.google.com/[YOUR PERSONAL G+ PROFILE HERE]
        ],
    ],
    
    /* -----------------------------------------------------------------
     |  Webmaster Tools
     | -----------------------------------------------------------------
     */
    'webmasters'  => [
        'google'    => 'XnisKkXHS8nS9RPVbfNDdrZoRUOI2edivik67udQ5es',
        'bing'      => '',
        'alexa'     => '',
        'pinterest' => '',
        'yandex'    => '',
    ],
    
    /* -----------------------------------------------------------------
     |  Open Graph
     | -----------------------------------------------------------------
     */
    'open-graph'  => [
        'enabled'     => true,
        'prefix'      => 'og:',
        'type'        => 'website',
        'title'       => 'Atap Dingin Rooftop®',
        'description' => 'The best solution for your roofing investment, We are proud to offer Rooftop high performance PVC roofing solutions representing only the finest manufacturers of equipment, with the technical know-how and support required, to provide total roofing peace of mind from project design to completion andbeyond.',
        'site-name'   => 'Atap Dingin Rooftop®',
        'properties'  => [
            //
        ],
    ],
    
    /* -----------------------------------------------------------------
     |  Twitter
     | -----------------------------------------------------------------
     |  Supported card types : 'app', 'gallery', 'photo', 'player', 'product', 'summary', 'summary_large_image'.
     */
    'twitter'     => [
        'enabled' => false,
        'prefix'  => 'twitter:',
        'card'    => 'summary',
        'site'    => 'Username',
        'title'   => 'The best solution for your roofing investment, We are proud to offer Rooftop high performance PVC roofing solutions representing only the finest manufacturers of equipment, with the technical know-how and support required, to provide total roofing peace of mind from project design to completion andbeyond.',
        'metas'   => [
            //
        ],
    ],
    
    /* -----------------------------------------------------------------
     |  Analytics
     | -----------------------------------------------------------------
     */
    'analytics' => [
        'google' => '', // UA-XXXXXXXX-X
    ],
];
