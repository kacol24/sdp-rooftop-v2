<?php
return [
    'calculator' => [
        'priceOpaque'    => 130000,
        'priceSemiTR'    => 165000,
        'priceRoofseal'  => 25000,
        'priceTopridge'  => 85000,
        'effectiveWidth' => 0.77,
        'roofsealPerBag' => 40,
    ],
    
    'social_medias' => [
        [
            'icon'  => 'fa-facebook',
            'url'   => 'https://www.facebook.com/atapdingin.SDP',
            'title' => 'Facebook',
        ],
        [
            'icon'  => 'fa-instagram',
            'url'   => 'https://www.instagram.com/rooftop.id/',
            'title' => 'Instagram',
        ],
        [
            'icon'  => 'fa-youtube',
            'url'   => 'https://www.youtube.com/channel/UCgRgBPNRx5IN6nKWFhFT0_w',
            'title' => 'YouTube',
        ],
    ],
    
    'contact_email' => 'marketing@sumberdjajaperkasa.com',
    
    'wp_url'   => env('WP_URL'),
    'cache'    => env('CACHE_POSTS', true),
    'cdn_path' => env('CDN_PATH', 'https://ce8b0600c.cloudimg.io/'),
];