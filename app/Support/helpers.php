<?php

use Illuminate\Support\Facades\URL;

if (! function_exists('localized_route')) {
    function localized_route($routeName)
    {
        return LaravelLocalization::getURLFromRouteNameTranslated(current_locale(), 'routes.' . $routeName);
    }
}

if (! function_exists('current_locale')) {
    function current_locale()
    {
        return LaravelLocalization::getCurrentLocale();
    }
}

if (! function_exists('cdn')) {
    function cdn($path, $cdn = false)
    {
        if (config('app.env') !== 'production') {
            return asset($path);
        }
        
        if ($cdn) {
            return URL::assetFrom($cdn, $path);
        }
        
        return URL::assetFrom(config('sdp.cdn_path') . 'cdn/n/n', asset($path));
    }
}

if (! function_exists('ci')) {
    function ci($path, $transformation)
    {
        if (config('app.env') !== 'production') {
            return URL::assetFrom(config('sdp.cdn_path') . $transformation, URL::assetFrom('https://rooftop.co.id', $path));
        }
        
        return URL::assetFrom(config('sdp.cdn_path') . $transformation, asset($path));
    }
}