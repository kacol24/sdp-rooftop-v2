<?php

namespace App\Models;

class Faq
{
    public $question;
    
    public $answer;
    
    public function __construct($parsed)
    {
        $this->question = $parsed['q'];
        $this->answer = $parsed['a'];
    }
}