<?php

namespace App\Models;

class ProjectCategory
{
    public $name;
    
    public $slug;
    
    public function __construct($parsed, $withProjects = true)
    {
        $this->name = $parsed['category_name'];
        $this->slug = $parsed['category'];
        if ($withProjects) {
            $this->projects = collect();
            foreach ($parsed['projects'] as $project) {
                $this->projects->push(new Project(array_merge($project, $parsed), $withCategory = false));
            }
        }
    }
}