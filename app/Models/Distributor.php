<?php

namespace App\Models;

class Distributor
{
    public $name;
    
    public $location;
    
    public $details;
    
    public function __construct($parsed)
    {
        $this->name = $parsed['name'];
        $this->location = $parsed['location'];
        $this->details = $parsed['details'];
    }
}