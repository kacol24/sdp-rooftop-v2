<?php

namespace App\Models;

class Slide
{
    public $image;
    
    public $cta;
    
    public $extra;
    
    public function __construct($parsed)
    {
        if (is_array($parsed)) {
            $this->image = $parsed['image'];
            if (isset($parsed['cta'])) {
                $this->cta = $parsed['cta'];
            }
            if (isset($parsed['extra'])) {
                $this->extra = $parsed['extra'];
            }
        } else {
            $this->image = $parsed['image'];
        }
    }
}