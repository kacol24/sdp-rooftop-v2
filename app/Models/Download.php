<?php

namespace App\Models;

class Download
{
    public $name;
    
    public $path;
    
    public function __construct($parsed)
    {
        $this->name = $parsed['filename'];
        $this->path = $parsed['filepath'];
    }
}