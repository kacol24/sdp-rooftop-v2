<?php

namespace App\Models;

use Carbon\Carbon;

class Post
{
    public $title;
    
    public $slug;
    
    public $body;
    
    public $excerpt;
    
    public $is_sticky;
    
    public $created_at;
    
    public function __construct($data)
    {
        $this->title = $data['title'];
        $this->slug = str_slug($this->title);
        $this->is_sticky = isset($data['is_sticky']) ? $data['is_sticky'] : false;
        $this->featured_image = $data['featured_image'];
        if (isset($data['excerpt'])) {
            $this->excerpt = $data['excerpt'];
        }
        if (isset($data['body'])) {
            $this->body = $data['body'];
        }
        if (isset($data['images'])) {
            $this->images = $data['images'];
        }
        $this->created_at = Carbon::createFromTimestamp($data['created_at']);
    }
}