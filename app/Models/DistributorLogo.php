<?php

namespace App\Models;

class DistributorLogo
{
    public $name;
    
    public $url;
    
    public function __construct($parsed)
    {
        $this->name = $parsed['name'];
        $this->url = $parsed['url'];
    }
}