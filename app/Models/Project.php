<?php

namespace App\Models;

class Project
{
    public $title;
    
    public $description;
    
    public $distributor;
    
    public $location;
    
    public $date;
    
    public $project_folder;
    
    public $featured_image;
    
    public $images;
    
    public function __construct($parsed, $withCategory = true)
    {
        $this->title = $parsed['title']; // required
        $this->description = isset($parsed['description']) ? $parsed['description'] : null;
        $this->distributor = isset($parsed['distributor']) ? $parsed['distributor'] : null;
        $this->location = isset($parsed['location']) ? $parsed['location'] : null;
        $this->date = isset($parsed['date']) ? $parsed['date'] : null;
        $this->project_folder = $parsed['project_folder']; // required
        $this->featured_image = $parsed['featured_image']; // required
        $this->images = collect($parsed['images']);
        if ($withCategory) {
            $this->category = new ProjectCategory($parsed, $withProjects = false); // required
        }
    }
}