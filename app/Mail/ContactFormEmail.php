<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactFormEmail extends Mailable
{
    use Queueable, SerializesModels;
    
    public $formData;
    
    /**
     * Create a new message instance.
     *
     * @param $form
     */
    public function __construct($form)
    {
        $this->formData = (object) $form;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $recipient = config('sdp.contact_email');
        //$recipient = 'kacol.bot@gmail.com';
        
        return $this->markdown('emails.contact-form')
                    ->to($recipient)
                    ->subject('[ROOFTOP CONTACT FORM]' . ($this->formData->subject ? ' ' . \title_case($this->formData->subject) : ''));
    }
}
