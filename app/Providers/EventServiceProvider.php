<?php

namespace App\Providers;

use App\Events\ContactFormSubmitted;
use App\Listeners\EmailContactForm;
use App\Listeners\FlashEmailSuccess;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Mail\Events\MessageSent;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ContactFormSubmitted::class => [
            EmailContactForm::class,
        ],
        MessageSent::class          => [
            FlashEmailSuccess::class,
        ],
    ];
    
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
