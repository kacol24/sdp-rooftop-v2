<?php

namespace App\Providers;

use App\Repositories\Cache\CachePostDecorator;
use App\Repositories\Contracts\PostRepository;
use App\Repositories\WordpressPostRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bindInterface('Project');
        $this->bindInterface('ProjectCategory');
        $this->bindInterface('Distributor');
        $this->bindInterface('Download');
        $this->bindInterface('Slide');
        $this->bindInterface('Faq');
        $this->bindInterface('DistributorLogo');
        $this->bindInterface('Post');
        //$this->app->bind(PostRepository::class, function () {
        //    $repository = new WordpressPostRepository();
        //    if (config('sdp.cache') == false) {
        //        return $repository;
        //    }
        //
        //    return new CachePostDecorator($repository);
        //});
    }
    
    private function bindInterface($name)
    {
        $this->app->bind(
            'App\\Repositories\\Contracts\\' . $name . 'Repository',
            'App\\Repositories\\Yaml' . $name . 'Repository'
        );
    }
}
