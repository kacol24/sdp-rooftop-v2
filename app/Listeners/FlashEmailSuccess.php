<?php

namespace App\Listeners;

class FlashEmailSuccess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle($event)
    {
        session()->flash('alert', [
            'type'    => 'alert-success',
            'message' => trans('general.email-sent'),
            'timeout' => true,
        ]);
    }
}
