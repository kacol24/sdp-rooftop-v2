<?php

namespace App\Services;

use App\Services\Calculator\AmountOfOpaqueCalculation;
use App\Services\Calculator\AmountOfRatioOperands;
use App\Services\Calculator\AmountOfSemiTRCalculation;
use App\Services\Calculator\RequiredAmountOfRoofsealBagCalculation;
use App\Services\Calculator\RequiredAmountOfRoofsealBagOperands;
use App\Services\Calculator\RequiredAmountOfRoofsealCalculation;
use App\Services\Calculator\RequiredAmountOfRoofsealOperands;
use App\Services\Calculator\RequiredAmountOfSheetsCalculation;
use App\Services\Calculator\RequiredAmountOfSheetsOperands;
use App\Services\Calculator\RequiredAmountOfTopRidgeCalculation;
use App\Services\Calculator\RequiredRooftopLengthCalculation;
use App\Services\Calculator\RequiredRooftopLengthOperands;
use App\Services\Calculator\TotalCostEstimateCalculation;
use App\Services\Calculator\TotalRoofsealCostCalculation;
use App\Services\Calculator\TotalTopRidgeCostCalculation;

class CalculatorService
{
    private $operands;
    
    public function __construct($operands)
    {
        $this->operands = $operands;
    }
    
    public function calculate()
    {
        $results = collect();
        
        $requiredRooftopLength = $this->run(RequiredRooftopLengthCalculation::class);
        $this->operands['requiredRooftopLength'] = $requiredRooftopLength;
        
        $requiredAmountOfSheets = $this->run(RequiredAmountOfSheetsCalculation::class);
        $this->operands['requiredAmountOfSheets'] = $requiredAmountOfSheets;
        
        $amountOfOpaque = $this->run(AmountOfOpaqueCalculation::class);
        $this->operands['amountOfOpaque'] = $amountOfOpaque;
        
        $amountOfSemiTR = $this->run(AmountOfSemiTRCalculation::class);
        $this->operands['amountOfSemiTR'] = $amountOfSemiTR;
        
        $requiredAmountOfRoofseal = $this->run(RequiredAmountOfRoofsealCalculation::class);
        $this->operands['requiredAmountOfRoofseal'] = $requiredAmountOfRoofseal;
        
        $requiredAmountOfRoofsealBag = $this->run(RequiredAmountOfRoofsealBagCalculation::class);
        $this->operands['requiredAmountOfRoofsealBag'] = $requiredAmountOfRoofsealBag;
        
        $requiredAmountOfTopRidge = $this->run(RequiredAmountOfTopRidgeCalculation::class);
        $this->operands['requiredAmountOfTopRidge'] = $requiredAmountOfTopRidge;
        
        $totalCostEstimate = $this->run(TotalCostEstimateCalculation::class);
        $totalRoofsealCost = $this->run(TotalRoofsealCostCalculation::class);
        $totalTopRidgeCost = $this->run(TotalTopRidgeCostCalculation::class);
        
        $results->put('requiredRooftopLength', $requiredRooftopLength);
        $results->put('amountOfOpaque', $amountOfOpaque);
        $results->put('amountOfSemiTR', $amountOfSemiTR);
        $results->put('requiredAmountOfSheets', $amountOfOpaque + $amountOfSemiTR);
        $results->put('requiredAmountOfRoofseal', $requiredAmountOfRoofseal);
        $results->put('requiredAmountOfRoofsealBag', $requiredAmountOfRoofsealBag);
        $results->put('requiredAmountOfTopRidge', $requiredAmountOfTopRidge);
        
        $results->put('totalRoofsealCost', $totalRoofsealCost);
        $results->put('totalTopRidgeCost', $totalTopRidgeCost);
        $results->put('totalCostEstimate', $totalCostEstimate);
        $results->put('totalCostWithAccessories', $totalCostEstimate + $totalRoofsealCost + $totalTopRidgeCost);
        
        return $results;
    }
    
    private function run($calculation)
    {
        $calculation = new $calculation($this->operands);
        
        return $calculation->calculate();
    }
}