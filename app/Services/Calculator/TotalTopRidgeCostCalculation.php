<?php

namespace App\Services\Calculator;

class TotalTopRidgeCostCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        return $this->operands['requiredAmountOfTopRidge'] * config('sdp.calculator.priceTopridge');
    }
}