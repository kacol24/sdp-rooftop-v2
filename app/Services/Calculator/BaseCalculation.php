<?php

namespace App\Services\Calculator;

abstract class BaseCalculation implements Calculation
{
    protected $operands;
    
    public function __construct($operands)
    {
        $this->operands = $operands;
    }
}