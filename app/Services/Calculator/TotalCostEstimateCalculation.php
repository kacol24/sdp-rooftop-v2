<?php

namespace App\Services\Calculator;

class TotalCostEstimateCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        return ($this->costOpaque() * $this->operands['requiredRooftopLength']) + ($this->costSemiTR() * $this->operands['requiredRooftopLength']);
    }
    
    private function costOpaque()
    {
        return $this->operands['amountOfOpaque'] * config('sdp.calculator.priceOpaque');
    }
    
    private function costSemiTR()
    {
        return $this->operands['amountOfSemiTR'] * config('sdp.calculator.priceSemiTR');
    }
}