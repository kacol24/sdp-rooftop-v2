<?php

namespace App\Services\Calculator;

class RequiredAmountOfSheetsCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        if ($this->operands['roof_type'] == 'canopy') {
            return ceil($this->operands['roof_width'] / config('sdp.calculator.effectiveWidth'));
        }
        
        return ceil($this->operands['roof_length'] / config('sdp.calculator.effectiveWidth')) * 2;
    }
}