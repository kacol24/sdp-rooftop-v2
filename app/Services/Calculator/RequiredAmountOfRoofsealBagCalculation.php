<?php

namespace App\Services\Calculator;

class RequiredAmountOfRoofsealBagCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        return ceil($this->operands['requiredAmountOfRoofseal'] / config('sdp.calculator.roofsealPerBag'));
    }
}