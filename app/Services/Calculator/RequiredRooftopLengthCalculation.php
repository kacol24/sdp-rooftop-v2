<?php

namespace App\Services\Calculator;

class RequiredRooftopLengthCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        if ($this->operands['roof_type'] == 'canopy') {
            return round($this->operands['roof_length'], 1);
        }
        
        return round($this->operands['roof_width'] / cos(deg2rad($this->operands['slope_angle'])), 1) / 2;
    }
}