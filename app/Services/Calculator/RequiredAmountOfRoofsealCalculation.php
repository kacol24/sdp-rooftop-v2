<?php

namespace App\Services\Calculator;

class RequiredAmountOfRoofsealCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        if ($this->operands['roof_type'] == 'canopy') {
            return ceil($this->operands['roof_length'] * $this->operands['roof_width'] * 4);
        }
        
        return ceil($this->operands['requiredRooftopLength'] * $this->operands['roof_length'] * 4) * 2;
    }
}