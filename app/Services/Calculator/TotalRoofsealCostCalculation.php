<?php

namespace App\Services\Calculator;

class TotalRoofsealCostCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        return $this->operands['requiredAmountOfRoofsealBag'] * config('sdp.calculator.priceRoofseal');
    }
}