<?php

namespace App\Services\Calculator;

interface Calculation
{
    public function __construct($operands);
    
    public function calculate();
}