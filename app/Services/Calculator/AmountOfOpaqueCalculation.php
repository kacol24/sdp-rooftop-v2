<?php

namespace App\Services\Calculator;

class AmountOfOpaqueCalculation extends BaseCalculation implements Calculation
{
    public function calculate()
    {
        return ceil($this->operands['ratio_opaque'] / $this->totalRatio() * $this->operands['requiredAmountOfSheets']);
    }
    
    private function totalRatio()
    {
        return $this->operands['ratio_opaque'] + $this->operands['ratio_semiTR'];
    }
}