<?php

namespace App\Repositories\Cache;

use App\Repositories\Contracts\PostRepository;

class CachePostDecorator implements PostRepository
{
    /**
     * @var \App\Repositories\Contracts\PostRepository
     */
    private $postRepository;
    
    private $locale;
    
    private $entity;
    
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
        $this->locale = current_locale();
        $this->entity = 'posts';
    }
    
    public function all()
    {
        // TODO: Implement all() method.
    }
    
    public function findBySlug($slug)
    {
        return cache()->rememberForever(
            "{$this->locale}.{$this->entity}.post-{$slug}", function () use ($slug) {
            return $this->postRepository->findBySlug($slug);
        });
    }
    
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }
    
    public function search($query)
    {
        // TODO: Implement search() method.
    }
    
    public function paginated($page = 1, $perPage = 10)
    {
        return cache()->rememberForever(
            "{$this->locale}.{$this->entity}.paginated-{$page}-{$perPage}", function () use ($page, $perPage) {
            return $this->postRepository->paginated($page, $perPage);
        });
    }
}