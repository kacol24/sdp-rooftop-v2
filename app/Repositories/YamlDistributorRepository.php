<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 9/22/17
 * Time: 12:09
 */

namespace App\Repositories;

use App\Models\Distributor;
use App\Repositories\Contracts\DistributorRepository;

class YamlDistributorRepository extends BaseYamlRepository implements DistributorRepository
{
    protected $file = 'distributors.yml';
    
    protected $class = Distributor::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}