<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 9/22/17
 * Time: 12:09
 */

namespace App\Repositories;

use App\Models\Slide;
use App\Repositories\Contracts\SlideRepository;

class YamlSlideRepository extends BaseYamlRepository implements SlideRepository
{
    protected $file = 'slides.yml';
    
    protected $class = Slide::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}