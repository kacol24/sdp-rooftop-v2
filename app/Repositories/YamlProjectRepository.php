<?php

namespace App\Repositories;

use App\Models\Project;
use App\Repositories\Contracts\ProjectRepository;

class YamlProjectRepository extends BaseYamlRepository implements ProjectRepository
{
    protected $file = 'projects.yml';
    
    protected $class = Project::class;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function findBySlug($slug)
    {
        $project = $this->all()->filter(function ($value) use ($slug) {
            return $value->project_folder == $slug;
        });
        
        return $project->first();
    }
    
    protected function mapYaml($parsed)
    {
        $mapped = collect();
        foreach ($parsed as $projectCategory) {
            foreach ($projectCategory['projects'] as $project) {
                $mapped->push(new Project(array_merge($project, $projectCategory)));
            }
        }
        
        return $mapped;
    }
}