<?php

namespace App\Repositories;

use App\Models\DistributorLogo;
use App\Repositories\Contracts\DistributorLogoRepository;

class YamlDistributorLogoRepository extends BaseYamlRepository implements DistributorLogoRepository
{
    protected $file = 'distributor-logos.yml';
    
    protected $class = DistributorLogo::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}