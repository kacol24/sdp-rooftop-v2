<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 9/22/17
 * Time: 12:09
 */

namespace App\Repositories;

use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class BaseYamlRepository
{
    protected $file;
    
    protected $class;
    
    protected $repo;
    
    public function __construct()
    {
        $filePath = resource_path('contents/' . $this->file);
        try {
            $parsed = Yaml::parse(file_get_contents($filePath));
            $this->repo = $this->mapYaml($parsed);
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }
    
    protected function mapYaml($parsed)
    {
        $mapped = collect();
        foreach ($parsed as $row) {
            $mapped->push(new $this->class($row));
        }
        
        return $mapped;
    }
    
    public function all()
    {
        return $this->repo;
    }
}