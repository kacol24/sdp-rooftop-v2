<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\PostRepository;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;

class WordpressPostRepository implements PostRepository
{
    private $client;
    
    private $locale;
    
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => config('sdp.wp_url') . 'wp-json/wp/v2/',
        ]);
        $this->locale = current_locale() == 'en' ? 'en' : '';
    }
    
    public function all()
    {
        $query = http_build_query([
            '_embed'   => true,
            'lang'     => $this->locale,
            'per_page' => 100,
        ]);
        $response = $this->client->get('posts?' . $query);
        
        return $this->mapResponse($response);
    }
    
    private function mapResponse($response)
    {
        $data = json_decode($response->getBody()->getContents(), true);
        
        return collect(array_map(function ($value) {
            return new Post($value);
        }, $data));
    }
    
    public function paginated($page = 1, $perPage = 10)
    {
        try {
            $query = http_build_query([
                '_embed'   => true,
                'lang'     => $this->locale,
                'page'     => $page,
                'per_page' => $perPage,
            ]);
            $response = $this->client->get('posts?' . $query);
            $total = (int) $response->getHeader('X-WP-Total')[0];
            
            $results = $this->mapResponse($response);
            $posts = new LengthAwarePaginator($results, $total, $perPage);
            
            return $posts;
        } catch (\Exception $e) {
            return collect();
        }
    }
    
    public function findBySlug($slug)
    {
        $query = http_build_query([
            '_embed' => true,
            'lang'   => $this->locale,
            'slug'   => $slug,
        ]);
        $response = $this->client->get('posts?' . $query);
        
        return $this->mapResponse($response)->first() ?: abort(404);
    }
    
    public function findById($id)
    {
        // TODO: Implement findById() method.
    }
    
    public function search($query)
    {
        // TODO: Implement search() method.
    }
}