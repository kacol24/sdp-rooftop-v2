<?php

namespace App\Repositories;

use App\Models\Post;
use App\Repositories\Contracts\PostRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class YamlPostRepository extends BaseYamlRepository implements PostRepository
{
    protected $file = 'blog.yml';
    
    protected $class = Post::class;
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function findBySlug($slug)
    {
        $posts = $this->all();
        $post = $posts->filter(function ($value) use ($slug) {
            return str_slug($value->title) == $slug;
        });
        
        return $post->first();
    }
    
    public function paginated($page = 1, $perPage = 10)
    {
        $posts = $this->all()->sortByDesc('created_at');
        $items = $posts->slice(($page - 1) * $perPage, $perPage);
        $paginated = new LengthAwarePaginator($items, $posts->count(), $perPage, $page);
        
        return $paginated;
    }
    
    public function findByYear($year)
    {
        $posts = $this->all();
        $filtered = $posts->filter(function ($value) use ($year) {
            return str_slug($value->created_at->year) == $year;
        });
        
        return $filtered;
    }
}