<?php

namespace App\Repositories\Contracts;

interface BaseRepository
{
    public function all();
}