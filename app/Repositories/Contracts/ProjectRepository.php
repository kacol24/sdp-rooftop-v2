<?php

namespace App\Repositories\Contracts;

interface ProjectRepository extends BaseRepository
{
    public function findBySlug($slug);
}