<?php

namespace App\Repositories\Contracts;

interface PostRepository extends BaseRepository
{
    public function findBySlug($slug);
    
    public function findByYear($year);
    
    public function paginated($page = 1, $perPage = 10);
}