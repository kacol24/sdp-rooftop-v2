<?php

namespace App\Repositories;

use App\Models\Faq;
use App\Repositories\Contracts\FaqRepository;

class YamlFaqRepository extends BaseYamlRepository implements FaqRepository
{
    protected $file = 'faqs.yml';
    
    protected $class = Faq::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}