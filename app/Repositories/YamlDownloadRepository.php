<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 9/22/17
 * Time: 12:09
 */

namespace App\Repositories;

use App\Models\Download;
use App\Repositories\Contracts\DownloadRepository;

class YamlDownloadRepository extends BaseYamlRepository implements DownloadRepository
{
    protected $file = 'downloads.yml';
    
    protected $class = Download::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}