<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 9/22/17
 * Time: 13:21
 */

namespace App\Repositories;

use App\Models\ProjectCategory;
use App\Repositories\Contracts\ProjectCategoryRepository;

class YamlProjectCategoryRepository extends BaseYamlRepository implements ProjectCategoryRepository
{
    protected $file = 'projects.yml';
    
    protected $class = ProjectCategory::class;
    
    public function __construct()
    {
        parent::__construct();
    }
}