<?php

namespace App\Http\Middleware;

use Closure;
use Spatie\ResponseCache\Facades\ResponseCache;

class FlushCache
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('flush')) {
            ResponseCache::flush();
            $request->session()->flash('alert', [
                'type'    => 'alert-success',
                'message' => 'Response cache successfully cleared!',
            ]);
            return redirect()->to(url()->current());
        }
        
        return $next($request);
    }
}
