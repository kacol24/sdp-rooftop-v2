<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 5/21/18
 * Time: 20:18
 */

namespace App\Http\ViewComposers;

use App\Repositories\Contracts\PostRepository;
use Illuminate\View\View;

class PostArchivesComposer
{
    /**
     * @var \App\Repositories\Contracts\PostRepository
     */
    protected $postRepository;
    
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }
    
    public function compose(View $view)
    {
        $postsArchives = $this->postRepository->all()->groupBy(function ($post) {
            return $post->created_at->year;
        });
        $view->with('postArchives', $postsArchives);
    }
}