<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CostCalculation extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'required_rooftop_length'               => (double) $this->resource['requiredRooftopLength'],
            'required_amount_of_sheets'             => (int) $this->resource['requiredAmountOfSheets'],
            'required_amount_of_roofseal'           => (int) $this->resource['requiredAmountOfRoofseal'],
            'required_amount_of_roofseal_bag'       => (int) $this->resource['requiredAmountOfRoofsealBag'],
            'required_amount_of_topridge'           => (int) $this->resource['requiredAmountOfTopRidge'],
            'amount_of_opaque'                      => (int) $this->resource['amountOfOpaque'],
            'amount_of_semiTR'                      => (int) $this->resource['amountOfSemiTR'],
            'total_roofseal_cost'                   => (int) $this->resource['totalRoofsealCost'],
            'formatted_total_roofseal_cost'         => $this->formattedPrice($this->resource['totalRoofsealCost']),
            'total_topridge_cost'                   => (int) $this->resource['totalTopRidgeCost'],
            'formatted_total_topridge_cost'         => $this->formattedPrice($this->resource['totalTopRidgeCost']),
            'total_cost_estimate'                   => (int) $this->resource['totalCostEstimate'],
            'formatted_total_cost_estimate'         => $this->formattedPrice($this->resource['totalCostEstimate']),
            'total_cost_with_accessories'           => (int) $this->resource['totalCostWithAccessories'],
            'formatted_total_cost_with_accessories' => $this->formattedPrice($this->resource['totalCostWithAccessories']),
        ];
    }
    
    private function formattedPrice($value)
    {
        return 'Rp' . number_format($value, 2, ',', '.');
    }
}
