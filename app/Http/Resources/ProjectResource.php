<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title'          => $this->title,
            'description'    => $this->description,
            'distributor'    => $this->distributor,
            'location'       => $this->location,
            'date'           => $this->date,
            'project_folder' => $this->project_folder,
            'images'         => $this->mapImagesUrl($this->images),
            'category'       => $this->category->name[current_locale()],
        ];
    }
    
    private function mapImagesUrl($images)
    {
        return $images->map(function ($image) {
            return [
                'lqip' => \Croppa::url("images/manipulated/uploads/projects/{$this->category->slug}/{$this->project_folder}/{$image}", 400, 300, [
                    'quality' => 30,
                    'filters' => ['blur'],
                ]),
                'full' => asset("uploads/projects/{$this->category->slug}/{$this->project_folder}/{$image}"),
            ];
        });
    }
}
