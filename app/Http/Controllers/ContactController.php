<?php

namespace App\Http\Controllers;

use App\Events\ContactFormSubmitted;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('doNotCacheResponse');
    }
    
    public function index()
    {
        $this->seo()->setTitle(\trans('seo.contact.title'));
        
        return view('contact.index');
    }
    
    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'           => 'required',
            'phone'                => 'required',
            'email'                => 'required|email',
            'agreement'            => 'accepted',
            'g-recaptcha-response' => 'required|captcha',
        ])->validate();
        
        event(new ContactFormSubmitted($request->except(['g-recaptcha-response', 'agreement'])));
        
        return back();
    }
}
