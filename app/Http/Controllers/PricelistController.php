<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\DownloadRepository;

class PricelistController extends Controller
{
    public function index(DownloadRepository $downloadRepository)
    {
        $this->seo()->setTitle(\trans('seo.pricelist.title'));
        
        $downloads = $downloadRepository->all();
        
        return view('pricelist.index', compact('downloads'));
    }
}
