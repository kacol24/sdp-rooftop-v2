<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\SlideRepository;
use Arcanedev\SeoHelper\Entities\Title;

class HomeController extends Controller
{
    public function index(SlideRepository $slideRepository)
    {
        $title = new Title();
        $title->set(\trans('seo.home.title'))
              ->setSiteName(\config('seo-helper.title.site-name'))
              ->setSeparator(' | ')
              ->setMax(250)
              ->setLast();
        $this->seoMeta()->title($title);
        
        $slides = $slideRepository->all();
        
        return view('home.index', compact('slides'));
    }
}
