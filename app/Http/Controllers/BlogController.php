<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\PostRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(PostRepository $postRepository, Request $request)
    {
        $this->seo()->setTitle(\trans('seo.blog.title'));
        
        if ($request->has('cache_flush')) {
            cache()->flush();
        }
        $posts = $postRepository->paginated($page = $request->get('page', 1));
        if (count($posts)) {
            $posts->withPath(route('blog.index'));
        }
        
        return view('blog.index', compact('posts'));
    }
    
    public function show(PostRepository $postRepository, $slug)
    {
        $post = $postRepository->findBySlug($slug);
        
        return view('blog.show', compact('post'));
    }
}
