<?php

namespace App\Http\Controllers;

class ProductController extends Controller
{
    public function index()
    {
        $this->seo()->setTitle(\trans('seo.product.title'));
        
        return view('product.index');
    }
}
