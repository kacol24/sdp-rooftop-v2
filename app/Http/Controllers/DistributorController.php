<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\DistributorRepository;

class DistributorController extends Controller
{
    public function index(DistributorRepository $distributorRepository)
    {
        $this->seo()->setTitle(\trans('seo.distributor.title'));
        
        $distributors = $distributorRepository->all();
        
        return view('distributors.index', compact('distributors'));
    }
}
