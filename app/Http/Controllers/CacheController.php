<?php

namespace App\Http\Controllers;

use Spatie\ResponseCache\Facades\ResponseCache;

class CacheController extends Controller
{
    public function flush()
    {
        session()->flash('alert', [
            'type'    => 'alert-success',
            'message' => 'Response cache successfully cleared!',
            'timeout' => true,
        ]);
        ResponseCache::clear();
        
        return redirect()->route('home.index');
    }
}
