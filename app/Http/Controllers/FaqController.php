<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\FaqRepository;

class FaqController extends Controller
{
    public function index(FaqRepository $faqRepository)
    {
        $this->seo()->setTitle(\trans('seo.faq.title'));
        
        $faqs = $faqRepository->all();
        
        return view('faq.index', compact('faqs'));
    }
}
