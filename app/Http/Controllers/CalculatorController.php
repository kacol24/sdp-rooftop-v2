<?php

namespace App\Http\Controllers;

use App\Http\Resources\CostCalculation;
use App\Services\CalculatorService;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
    public function index()
    {
        $this->seo()->setTitle(\trans('seo.calculator.title'));
        
        return view('calculator.index');
    }
    
    public function calculate(Request $request)
    {
        $calculator = new CalculatorService($request->except('_token'));
        $results = $calculator->calculate();
        
        return new CostCalculation($results);
    }
}
