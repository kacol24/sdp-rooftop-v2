<?php

namespace App\Http\Controllers;

class OurCompanyController extends Controller
{
    public function index()
    {
        $this->seo()->setTitle(\trans('seo.our-company.title'));
        
        return view('our-company.index');
    }
}
