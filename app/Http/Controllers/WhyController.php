<?php

namespace App\Http\Controllers;

class WhyController extends Controller
{
    public function index()
    {
        $this->seo()->setTitle(\trans('seo.why-rooftop.title'));
        
        return view('why-rooftop.index');
    }
    
    public function compare()
    {
        $this->seo()->setTitle(\trans('seo.why-rooftop.compare'));
        
        return view('why-rooftop.compare');
    }
}
