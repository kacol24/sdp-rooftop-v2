<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProjectResource;
use App\Repositories\Contracts\ProjectCategoryRepository;
use App\Repositories\Contracts\ProjectRepository;

class ProjectController extends Controller
{
    public function index(ProjectCategoryRepository $projectCategoryRepository)
    {
        $this->seo()->setTitle(\trans('seo.projects.title'));
        
        $projectCategories = $projectCategoryRepository->all();
        
        return view('projects.index', compact('projectCategories'));
    }
    
    public function show(ProjectRepository $projectRepository, $slug)
    {
        $project = $projectRepository->findBySlug($slug);
        
        return new ProjectResource($project);
    }
}
